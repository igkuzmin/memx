#include "../src/histogram.h"

#include <gtest/gtest.h>
#include <cmath>

namespace memx {

namespace {
struct print {
    const std::vector<Value> & m_values;

    print(const Histogram & his) : m_values(his.get_density()) {}

    print(const std::vector<Value> & values) : m_values(values) {}
    friend std::ostream & operator<<(std::ostream & strm, const print & o){
        const auto & values = o.m_values;
        strm << "{";
        for (size_t i = 0; i < values.size(); ++i) {
            const auto & v = values[i];
            strm << '[' << v.value << ',' << v.count << "]";

            if (i + 1 == values.size()) {
                strm << "}\n";
            }
            else {
                strm << ", ";
            }
        }
        return strm;
    }
};

}

TEST(HistogramTest, basic)
{
    Histogram his;
    for (size_t i = 0; i < 100; ++i) {
        his.add_sample(i);
    }
    EXPECT_EQ(100, his.get_num());
    EXPECT_DOUBLE_EQ(49.5, his.get_mean());
    EXPECT_DOUBLE_EQ(833.25, his.get_variance());
    EXPECT_GE(0.01, std::abs(his.get_deviation() - 28.86));
}

//TEST(HistogramTest, compress)
//{
//    Histogram his;
//    for (size_t i = 1; i < 10; ++i) {
//        his.add_sample(i);
//    }
//    EXPECT_EQ(9, his.get_values().size());
//    ASSERT_DOUBLE_EQ(5, his.get_mean());
//
//    his.compress_values(3);
//    EXPECT_EQ(3, his.get_values().size());
//    ASSERT_DOUBLE_EQ(5, his.get_mean());
//}

//TEST(HistogramTest, basic_update)
//{
//    Histogram his(100);
//    for (size_t i = 0; i < 100; ++i) {
//        his.add_sample(i);
//    }
//    EXPECT_DOUBLE_EQ(10, his.get_values().size()) << "Expect values to be compressed.";
//    EXPECT_DOUBLE_EQ(49.5, his.get_mean());
//    ASSERT_GE(1, std::abs(825 - his.get_variance()));
//}

TEST(HistogramTest, density)
{
    Histogram his;
    for (size_t i = 1; i < 10; ++i) {
        his.add_sample(i);
    }
    const auto density = his.get_density_average(10);
    EXPECT_EQ(9, density.size()) << "There are less samples than buckets";
    for (size_t i = 0; i < density.size(); ++i) {
        EXPECT_EQ(1, density[i].count);
        EXPECT_DOUBLE_EQ(i + 1, density[i].value);
    }

    for (size_t i = 1; i < 10; ++i) {
        his.add_sample(5);
    }

    const auto density_10 = his.get_density_average(10);
    EXPECT_DOUBLE_EQ(density_10[0].count, density_10[1].count);
    EXPECT_DOUBLE_EQ(density_10[0].count, density_10[1].count);
    EXPECT_EQ(10, density_10[4].count);

    const auto density_5 = his.get_density_average(5);
    ASSERT_EQ(5, density_5.size());

    EXPECT_EQ(2,  density_5[0].count);
    EXPECT_EQ(2,  density_5[1].count);
    EXPECT_EQ(11, density_5[2].count);
    EXPECT_EQ(2,  density_5[3].count);
    EXPECT_EQ(1,  density_5[4].count);

    EXPECT_DOUBLE_EQ(1.5,     density_5[0].value);
    EXPECT_DOUBLE_EQ(3.5,     density_5[1].value);
    EXPECT_DOUBLE_EQ(7.5,     density_5[3].value);
    EXPECT_DOUBLE_EQ(9,       density_5[4].value);
    EXPECT_GE(0.01, std::abs(5.09 - density_5[2].value));
}

TEST(HistogramTest, merge)
{
    Histogram his1;
    for (size_t i = 0; i < 5; ++i) {
        his1.add_sample(i);
    }
    Histogram his2;
    his2
        .add_sample(5)
        .add_sample(7, 2)
        .add_sample(3);

    his1.merge(his2);
    ASSERT_EQ(9, his1.get_num());
    const auto & dens = his1.get_density();
    ASSERT_EQ(7, dens.size());

    EXPECT_EQ(1, dens[0].count);
    EXPECT_EQ(1, dens[1].count);
    EXPECT_EQ(1, dens[2].count);
    EXPECT_EQ(2, dens[3].count);
    EXPECT_EQ(1, dens[4].count);
    EXPECT_EQ(1, dens[5].count);
    EXPECT_EQ(2, dens[6].count);

    EXPECT_DOUBLE_EQ(0, dens[0].value);
    EXPECT_DOUBLE_EQ(1, dens[1].value);
    EXPECT_DOUBLE_EQ(2, dens[2].value);
    EXPECT_DOUBLE_EQ(3, dens[3].value);
    EXPECT_DOUBLE_EQ(4, dens[4].value);
    EXPECT_DOUBLE_EQ(5, dens[5].value);
    EXPECT_DOUBLE_EQ(7, dens[6].value);
}

TEST(HistogramTest, percentile)
{
    Histogram his;
    for (size_t i = 0; i < 10; ++i) {
        his.add_sample(i);
    }
    EXPECT_EQ(4, his.get_percentile(50));
    EXPECT_EQ(8, his.get_percentile(90));
    EXPECT_EQ(9, his.get_percentile(100));
}

TEST(HistogramTest, median)
{
    Histogram his;
    for (size_t i = 1; i < 10; ++i) {
        his.add_sample(i);
    }
    EXPECT_EQ(5, his.get_median()) << print(his);
    his.add_sample(9);
    EXPECT_EQ(5, his.get_median()) << print(his);
    his.add_sample(9);
    EXPECT_EQ(6, his.get_median()) << print(his);
}

TEST(HistogramTest, number)
{
    Histogram his;
    const size_t N = 1000;
    for (size_t i = 0; i < N; ++i) {
        his.add_sample(i%10);
    }
    EXPECT_EQ(N, his.get_num());
    size_t n = 0;
    for (const auto & v: his.get_density()) {
        n += v.count;
    }
    EXPECT_EQ(N, n);
}

//TEST(HistogramTest, python)
//{
//    Histogram hist;
//    for (size_t i = 0; i < 100; ++i) {
//        hist.add_sample(rand());
//    }
//    std::cout << std::fixed << std::setprecision(4);
//    std::cout << "# mean: " << hist.get_mean() << std::endl;
//    std::cout << "# stddev: " << hist.get_deviation() << std::endl;
//    std::cout << "# variance: " << hist.get_variance() << std::endl;
//
//    std::cout << "import numbpy as np\n";
//    std::cout << "a = []\n";
//    for (const auto v: hist.get_density()) {
//        for (size_t i = 0; i < v.count; ++i) {
//            std::cout <<  "a.append(" << std::lround(v.value) % 1000 << ")\n";
//        }
//    }
//    std::cout << "a_np = np.array(a)\n";
//    std::cout << "print(a_np.mean())\n";
//    std::cout << "print(a_np.std())\n";
//    std::cout << "print(a_np.var())\n";
//}

}
