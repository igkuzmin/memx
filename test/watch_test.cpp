#include "../src/watch.h"

#include <gtest/gtest.h>

#include <chrono>
#include <thread>

namespace memx {

uint64_t diff(uint64_t lhs, uint64_t rhs)
{ return lhs > rhs ? lhs - rhs : rhs - lhs; }


class StopWatchTest : public ::testing::Test
{
public:
    static void SetUpTestCase() {
        TSC_init_rate();
    }
};

TEST_F(StopWatchTest, running) {
    StopWatch sw;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    ASSERT_GE(1, diff(10, sw.running().msecs()));
}

TEST_F(StopWatchTest, running_sum)
{
    StopWatch sw;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    auto t = sw.stop();
    ASSERT_GE(1, diff(10, t.msecs())) << "ms: " << t.msecs();

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    ASSERT_EQ(t.nsecs(), sw.nsecs());

    sw.start();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    t = sw.running();
    ASSERT_GE(1, diff(50, t.msecs())) << "us: " << t.usecs();
    t = sw.stop();
    ASSERT_GE(1, diff(50, t.msecs()));
    ASSERT_GE(1, diff(10 + 50, sw.msecs()));
}

}
