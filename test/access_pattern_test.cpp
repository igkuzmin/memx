#include "../src/access_pattern.h"
#include "../src/histogram.h"
#include "../src/Config.h"
#include <gtest/gtest.h>

namespace memx {

TEST(AccessPatternTest, unimodal)
{
    static const size_t ws = 1000;
    Config cfg; cfg
        .set_seed(3)
        .set_access_pattern("unimodal")
        .set_working_set_size(ws)
        .set_mean(.5)
        .set_stddev(0.13)
        .set_alignment(8);

    generator_t random_generator(333);
    auto p = IAccessPattern::create(cfg, random_generator);
    Histogram h;
    ASSERT_TRUE(p) << "Can't populate unimodal access pattern";
    for (size_t i = 0; i < 10000; ++i) {
        h.add_sample(p->next_offset());
    }

    ASSERT_NEAR(h.get_deviation() / ws, 0.13, 0.01);
    ASSERT_NEAR(h.get_mean() / ws, 0.5, 0.01);
}

TEST(AccessPatternTest, uniform)
{
    static const size_t ws = 1000;
    Config cfg; cfg
        .set_access_pattern("uniform")
        .set_seed(3)
        .set_working_set_size(ws)
        .set_alignment(8);

    generator_t random_generator(333);
    auto p = IAccessPattern::create(cfg, random_generator);
    Histogram h;
    ASSERT_TRUE(p) << "Can't populate uniform access pattern";
    for (size_t i = 0; i < 10000; ++i) {
            const auto o = p->next_offset();
        h.add_sample(o);
    }

    ASSERT_NEAR(h.get_mean() / ws, 0.5, 0.01);
    ASSERT_NEAR(h.get_deviation() / ws, 0.29, 0.01);
}

}
