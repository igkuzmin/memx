#DEBUG ?= YES
ECHO := echo
CXXFLAGS :=-std=c++17 -pthread -fopenmp
CXXFLAGS +=-fno-omit-frame-pointer -march=native

__NODE := $(strip $(shell uname -n))

ifeq (${DEBUG},YES)
  $(info ## NB! Building Debug binary')
  CXXFLAGS +=-g
  LDFLAGS +=-g
else ifeq (${DEBUG},MIX)
  CXXFLAGS +=-g -O2
  LDFLAGS +=-g
else
  CXXFLAGS +=-O2
endif

memx.SRCs := $(wildcard src/*.cpp)
memx.OBJs := $(memx.SRCs:.cpp=.o)
memx.LIB := -ljsoncpp -lnuma -Wl,-rpath=$(shell pwd)

ifneq "$(wildcard /usr/include/jsoncpp)" ""
    CXXFLAGS += -I/usr/include/jsoncpp
endif

test.SRCs := $(wildcard test/*.cpp)
test.SRCs += src/histogram.cpp src/RDTSC.cpp src/access_pattern.cpp
test.OBJs := $(test.SRCs:.cpp=.o)
test.LIB  := -lgtest -lgtest_main

OBJs := ${memx.OBJs} ${test.OBJs}
Makefile.DEP := Makefile $(wildcard *.mf)

memx: ${memx.OBJs} ${Makefile.DEP} # ${HDRs}
	@${ECHO} " #  linking $@..."
	@${CXX} -o $@ ${CXXFLAGS} ${LDFLAGS} ${memx.LIB} ${memx.OBJs}

memx.test: ${test.OBJs} ${Makefile.DEP}
	@${ECHO} " #  linking $@..."
	@${CXX} -o $@ ${CXXFLAGS} ${test.LIB} ${test.OBJs}

ifeq (${__NODE},xps)
  NUMACTL ?= -C0-6 -m0
  OMP_NUM_THREADS ?= 6
else ifeq (${__NODE},nvram)
  NUMACTL ?= -C16-31 -m1,3
  OMP_NUM_THREADS ?= 16
endif

export OMP_NUM_THREADS NUMACTL

MEMX.seed ?= 1
MEMX.pattern ?= uniform
MEMX.size ?= 1024
MEMX.limit ?= 100000000
MEMX.time ?= 60
MEMX.opt ?= -v
MEMX.bytes ?= 8
MEMX.alignment ?= ${MEMX.bytes}
MEMX.read ?= 'empty'
MEMX.write ?= 'empty'
MEMX.rr ?= 50
MEMX.mean ?= .3
MEMX.stddev ?= .015
MEMX.mean_2 ?= .6
MEMX.stddev_2 ?= ${MEMX.stddev}
MEMX.rr2 ?= 100
MEMX.pmap ?= memx.pmap
MEMX.access ?= ''

BFS.scale ?= 24
BFS.trials ?= 50
bfs:
	@${ECHO} " # OMP_NUM_THREADS=${OMP_NUM_THREADS}"
	@${ECHO} " # NUMACTL=${NUMACTL}"
	@${ECHO} " # BFS.scale=${BFS.scale}"
	@${ECHO} " # BFS.trials=${BFS.trials}"
	@numactl ${NUMACTL} ${RUNNER} -- \
	   	../gapbs/bfs \
		-g ${BFS.scale} \
		-n ${BFS.trials} \
		|tee bfs.log

.PHONY: run.gapbs.bfs

run run.log memx.pmap: memx
	@${ECHO} " # OMP_NUM_THREADS=${OMP_NUM_THREADS}"
	@${ECHO} " # NUMACTL=${NUMACTL}"
	@${ECHO} " # RUNNER=${RUNNER}"
	@${ECHO} " # MEMX.pattern=${MEMX.pattern}"
	@${ECHO} " # MEMX.seed=${MEMX.seed}"
	@${ECHO} " # MEMX.size=${MEMX.size}"
	@${ECHO} " # MEMX.limit=${MEMX.limit}"
	@${ECHO} " # MEMX.time=${MEMX.time}"
	@${ECHO} " # MEMX.rr=${MEMX.rr}"
	@${ECHO} " # MEMX.bytes=${MEMX.bytes}"
	@${ECHO} " # MEMX.alignment=${MEMX.alignment}"
	@${ECHO} " # MEMX.read=${MEMX.read}"
	@${ECHO} " # MEMX.write=${MEMX.write}"
	@${ECHO} " # MEMX.pmap=${MEMX.pmap}"
	@${ECHO} " # MEMX.access=${MEMX.access}"
	@${ECHO} " # MEMX.opt=${MEMX.opt}"
	@numactl ${NUMACTL} -- ${RUNNER} -- ./memx \
		--access_pattern ${MEMX.pattern} \
		--working_set ${MEMX.size} \
		--limit_operations ${MEMX.limit} \
		--limit_seconds ${MEMX.time} \
		--seed ${MEMX.seed} \
		--read_ratio ${MEMX.rr} \
		--write_mode ${MEMX.write} \
		--read_mode ${MEMX.read} \
		--mean ${MEMX.mean} \
		--stddev ${MEMX.stddev} \
		--mean_2 ${MEMX.mean_2} \
		--stddev_2 ${MEMX.stddev_2} \
		--read_ratio_2 ${MEMX.rr2} \
		--touch_bytes ${MEMX.bytes} \
		--alignment ${MEMX.alignment} \
		--pmap_file ${MEMX.pmap} \
		--access_log ${MEMX.access} \
		${MEMX.opt} | tee run.log

run.bimodal: memx
	@${ECHO} " # bimodal"
	@make run \
	   	MEMX.pattern=bimodal \
	   	MEMX.rr=100 \
	   	MEMX.mean=.3   MEMX.stddev=.01 \
		MEMX.mean_2=.6 MEMX.stddev_2=.01 \
	   	MEMX.read_ratio_2=0

run.unimodal: memx
	@${ECHO} " # $@"
	@make run \
	   	MEMX.pattern=unimodal \
		MEMX.write=mfence \
		MEMX.rr=0

run.uniform: memx
	@${ECHO} " # $@"
	@make MEMX.pattern=uniform run

test-suit: memx
	@numactl ${NUMACTL} -- ./memx --test-suit example.js

#PERF ?= "-e cycles:pp"
#perf: memx
#	@${ECHO} " #  recording..."
#	@perf record ${PERF} -- make run


#@perf record --phys-data --data -e mem-stores:pp,mem-loads:pp -- make run

PERF.data ?= ./perf.data
PERF.events ?= mem_inst_retired.all_loads:pp,mem_inst_retired.all_stores:pp
PERF.count ?= 24
PERF.runner := perf record \
   	--data \
	-e  ${PERF.events} \
	-o ${PERF.data} \
	-m +256M \
   	--count ${PERF.count} \
	${PERF.ext}
#--phys-data 

PEBS.args := \
	MEMX.pattern=bimodal \
	MEMX.rr=100 \
   	MEMX.rr2=100 \
	MEMX.stddev=0.001 \
   	MEMX.time=1 \
	MEMX.limit=0

PEBS.run ?= run

pebs ${PERF.data}:
	@${ECHO} " # Collecting PEBS...."
	@${ECHO} " # PERF.events=${PERF.events}"
	@${ECHO} " # PERF.count=${PERF.count}"
	@make ${PEBS.run} RUNNER="${PERF.runner}" ${PEBS.args}
	@# -- make run

pebs.bfs:
	@make pebs PEBS.run=bfs

strace trace.txt:
	@${ECHO} " # collecting PEBS traces..."
	@strace -o trace.txt -f -- make pebs

iomem: /proc/cpuinfo
	@${ECHO} Retriving memory map..
	@sudo cat /proc/iomem > $@

script: ${PERF.data}
	@perf script -s perf-script.py ${SCRIPT} |tee script.log
	@#perf script -s perf-script.py --header -c memx ${SCRIPT}

report: ${PERF.data}
	@perf report

call-graph:
	@make PERF="--call-graph dwarf" report

annotate: ${PERF.data}
	#@perf annotate -Mintel -s _ZN4memx15PerformanceTest3runEv._omp_fn.0
	@perf annotate -Mintel --stdio  --stdio-color


TESTFLAGS := $(if ${TESTS},--gtest_filter="${TESTS}",)
test: memx.test
ifeq (${TESTS},NO)
	@echo " #  Skipping tests run"
else
	@echo " #  Running tests..."
	@./memx.test ${TESTFLAGS}
endif

memx.asm: memx
	@${ECHO} " #  building $@..."
	@objdump -Mintel -S $< |c++filt >$@

clean:
	@rm -rf memx memx.asm memx.test ${OBJs} $(OBJs:.o=.d)

clean_all: clean

%.o: %.cpp
	@${ECHO} " #  building $@..."
	@${CXX} ${CXXFLAGS} -c $*.cpp -o $*.o
	@# the following lines generate dependency files for the target object
	@${CXX} -MM ${CXXFLAGS} $*.cpp > $*.d
	@mv -f $*.d $*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $*.d.tmp > $*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

ifneq (${MAKECMDGOALS},clean)
  -include $(OBJs:.o=.d)
endif

trace.bin ?= ${PERF.data}.bin

perf.data: ${PERF.data}
trace.bin: ${trace.bin}.xz
.PHONY: perf.data trace.bin

tags: .tags

.tags: $(memx.SRCs:%.cpp=%.d)
	@${ECHO} " #  building tags... ($^)"

memx.access: memx
	@make run MEMX.access=$@ ${PEBS.args}

accesses.txt: memx.access accesses.py
	@python accesses.py memx.access >$@

${trace.bin}.xz: perf-trace-write.py ${PERF.data}
	@${ECHO} "Generating ${trace.bin}"
	@perf script -s perf-trace-write.py -i ${PERF.data} ${trace.bin}
	@${ECHO} "Compressing ${trace.bin}"
	@rm -f ${trace.bin}.xz
	@xz ${trace.bin}

parse_trace: parse_trace.py ${trace.bin}.xz
	@python  $^
.PHONY: parse_trace

.PHONY: clean test run perf report call-graph test-suit clean_all strace tags run.bimodal run.unimodal accesses run.pebs
USE_PCM=
include libpcm.mf
include evaluate.mf


