#!/bin/bash

if [ "nvram" == "$(uname -n)" ]; then
    echo "nvram specific config"
    if [ "create" == "$1" ]; then
        sudo ndctl create-namespace --region region0 -M mem -m devdax
        sudo ndctl create-namespace --region region1 -M mem -m devdax
        exit 0
    fi

    sudo daxctl reconfigure-device -m system-ram dax0.0
    sudo daxctl reconfigure-device -m system-ram dax1.0

    #sudo mount -t tmpfs -o size=18G,mpol=bind:1,mode=777 tmpfs /mnt/ramdisk
    #sudo mount -t tmpfs -o size=200G,mpol=bind:3,mode=777 tmpfs /mnt/nvramdisk

    sudo swapoff /dev/sda5

    sudo sysctl kernel.numa_balancing=0
    sudo sysctl kernel.nmi_watchdog=0 
fi

echo -1 |sudo tee /proc/sys/kernel/perf_event_paranoid
echo 100|sudo tee /proc/sys/kernel/perf_cpu_time_max_percent # disable throttling
echo $((1024*1024))|sudo tee /proc/sys/kernel/perf_event_mlock_kb

echo 0 |sudo tee /proc/sys/kernel/randomize_va_space
echo 0 |sudo tee /proc/sys/kernel/kptr_restrict

