import os
import struct
import lzma

class Trace:
    REC = '!QbQQ'
    REC_sz = struct.calcsize(REC)

    def __init__(self, time = None, va = None, pa = None, op = ord('u')):
        self.op = op
        self.va = va
        self.pa = pa
        self.time = time

    def __repr__(self):
        time = va = pa = op = ''
        if self.time is not None and self.time != 0: time = "{}:".format(self.time)
        if self.va   is not None and self.va   != 0: va = "{:x}".format(self.va)
        if self.pa   is not None and self.pa   != 0: pa = "[{:x}]".format(self.pa)
        if self.op   is not None and self.op   != 0: op = ":{:c}".format(self.op)
        return "Trace<" + time + va + pa + op + ">"


    def serialize(self, fd):
        fd.write(struct.pack(self.REC, self.time, self.op, self.va, self.pa))
        #fd.write(self.time.to_bytes(8, 'little'))
        #fd.write(self.op.to_bytes(1, 'little'))
        #fd.write(self.va.to_bytes(8, 'little'))
        #fd.write(self.pa.to_bytes(8, 'little'))

    def deserialize(self, fd):
        #size = struct.calcsize(self.REC)
        buf = fd.read(self.REC_sz)
        n = len(buf)
        if n == self.REC_sz:
            (self.time, self.op, self.va, self.pa) = struct.unpack_from(self.REC, buf)
            return n
        if n != 0:
            raise Exception("Unexpected data termination")
        return 0

        #size = 0;

        #time = fd.read(8)
        #size += len(time)
        #self.time = int.from_bytes(time, 'little')

        #op = fd.read(1)
        #size += len(op)
        #self.op = int.from_bytes(op, 'little')

        #va = fd.read(8)
        #size += len(va)
        #self.va = int.from_bytes(va, 'little')

        #pa = fd.read(8)
        #size += len(pa)
        #self.pa = int.from_bytes(pa, 'little')

        return size

class TraceFileHeader:

    MAGIC = bytearray('TRACE', 'utf-8')
    HEADER_FORMAT = '!BQ'

    def __init__(self):
        self.version = 0
        self.records = 0

    def serialize(self, fd):
        fd.write(self.MAGIC)
        fd.write(struct.pack(self.HEADER_FORMAT,
                self.version,
                self.records))

    def deserialize(self, fd):
        magic = fd.read(len(TraceFileHeader.MAGIC))
        buf = fd.read(struct.calcsize(self.HEADER_FORMAT))
        (self.version, self.records) = struct.unpack(self.HEADER_FORMAT, buf)
        size = len(magic) + len(buf)

        if magic != TraceFileHeader.MAGIC:
            raise Exception("Unexpected file format (wrong magic)")

        return size

class TraceWriter:
    def __init__(self, filename = None):
        if filename is not None:
            self.open(filename)

    def open(self, filename):
        self.__fd = open(filename, 'wb')
        self.__header = TraceFileHeader()
        self.__header.serialize(self.__fd)

    def close(self):
        self.__fd.seek(0)
        self.__header.serialize(self.__fd)
        self.__fd.close()
        self.__fd = None

    def __del__(self):
        if self.__fd is not None:
            self.close()

    def add(self, trace):
        trace.serialize(self.__fd)
        self.__header.records += 1

class TraceReader:

    def __init__(self, filename = None):
        self.filename = filename
        self.header = TraceFileHeader()
        with TraceReader._open(filename) as fd:
            self.header.deserialize(fd)

    @staticmethod
    def _open(filename):
        with open(filename, 'rb') as fd:
            buf = fd.read(4)
            if buf == b'\xfd7zX':
                return lzma.open(filename)
        return open(filename, 'rb')

    def __iter__(self):
        return TraceReader.Iterator(self.filename)

    class Iterator:
        def __init__(self, filename):
            self.fd = fd = TraceReader._open(filename)
            self.header = TraceFileHeader()
            self._offset = self.header.deserialize(fd)

        def __next__(self):
            t = Trace()
            sz = t.deserialize(self.fd)
            if sz > 0: return t
            raise StopIteration

        def __del__(self):
            self.fd.close()

##### UNIT TESTS ##############################################################
if __name__ == '__main__':
    import unittest
    class TestTrace(unittest.TestCase):
        FILE = "trace.test"
        def test_smoke(self):
            writer = TraceWriter(TestTrace.FILE)
            traces = [ \
                    Trace(5,  0x1000, 0x1, ord('r')),
                    Trace(9,  0x1002, 0x1, ord('w')),
                    Trace(12, 0x2002, 0x2, ord('r')),
                    Trace(12, 0x2002, 0x2, ord('u')),
                    ]
            for t in traces:
                writer.add(t)
            writer.close()

            reader = TraceReader(TestTrace.FILE)
            self.assertEqual(3, reader.header.records)

            n = 0
            for t in reader:
                self.assertEqual(t.time, traces[n].time)
                self.assertEqual(t.va, traces[n].va)
                self.assertEqual(t.pa, traces[n].pa)
                self.assertEqual(t.op, traces[n].op)
                n += 1

        # -------------------------------------------------

        def __del__(self):
            os.unlink(TestTrace.FILE)

    unittest.main()
################################################################################
