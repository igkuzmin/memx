from __future__ import print_function

import os
import sys

from trace import TraceWriter, Trace

sys.path.append(os.environ['PERF_EXEC_PATH'] + \
	'/scripts/python/Perf-Trace-Util/lib/Perf/Trace')


def trace_begin():
    global trace_writer
    tracefile = "trace.bin"
    if len(sys.argv) > 1:
        tracefile = sys.argv[1]
    trace_writer = TraceWriter(tracefile)

def trace_end():
    pass

def process_event(param_dict):
    global trace_writer
    name       = param_dict['ev_name']
    sample     = param_dict['sample']
    phys_addr  = sample['phys_addr']
    addr       = sample['addr']
    ip         = sample['ip']
    time       = sample['time']

    if name.startswith('mem_inst_retired.all_loads'): #read
        op = 'r'
    elif name.startswith('mem_inst_retired.all_stores'): #write
        op = 'w'
    else:
        raise Exception("Unknown operation {}".format(name))

    trace_writer.add(Trace(time, addr, phys_addr, ord(op)))
