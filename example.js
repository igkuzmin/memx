{
    "seed" : 2048,
    "limit_seconds" : 15,
    "touch_bytes" : 8,
    "alignment" : 8,
    "log_level" : "warning",
    "working_set_size" : ["1Gb", "10Gb", "30Gb"],
    "num_threads" : [1, 2, 4, 8, 16],
    "read_ratio_variant": [
        {   "read_ratio" : 100},
        {
            "read_ratio" : [50, 0],
            "write_mode" : ["none", "nt", "mfence", "clwb"]
        }
    ],
    "access_pattern_variant" : [
        {   "access_pattern" : "uniform" },
        {
            "access_pattern" : "linear",
            "stride" : [8, 128, 246]
        }
    ]
}
