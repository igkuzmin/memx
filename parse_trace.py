import logging
import os
import sys

from logging import DEBUG, INFO
from record import AddressSpace, Record
from trace import Trace, TraceReader
from simulation import HeMemPEBS, PageTableScan, LRU, Baseline, Simulator, ActionSimulation, ClassifierStats

import pandas as pd

#WINDOW = 1000000000 / 30 # 30 per second
#DECAY_value = 5

#uniq_pages = {}

tracefile = 'trace.bin'
if len(sys.argv) > 1:
    tracefile = sys.argv[1]

class Accuracy: #(ActionSimulation):
    LOG = logging.getLogger("Accuracy")

    #CHECK_PERIOD        = 100000000 # 100ms
    HOT_THRESHOLD_RATIO = .5 # i.e. a page is classified as a hot
                             # if it is in the set of those that 
                             # accept HOT_THRESHOLD_RATIO of all
                             # accesses

    Baseline_DECAY = 1000     # event cap

    HeMem_INTERVAL = 1 * 10**6   # ms
    HeMem_PERIOD   = int(19997)
    heMem_EVENTS_THRASHOLD = 100  # fire statistics when at least X events happened

    lru_SCAN_PERIOD = 100  * 10**6 # ms
    lru_SWAP_PERIOD = 300  * 10**6 # ms

    pts_SAMPLE_PERIOD   = 500 * 10**6 # ms
    pts_SAMPLE_DURATION = 100  * 10**3 # μs


    def __init__(self):
        #ActionSimulation.__init__(self, self.CHECK_PERIOD, 'checker')
        self.baseline = Baseline(name="baseline")
        self.baseline.DECAY_THRESHOLD = self.Baseline_DECAY
        sims = []

        hemem = HeMemPEBS(self.HeMem_INTERVAL, self.HeMem_PERIOD, name = 'HeMemPEBS')
        hemem.PAGE_MASK = -1 << 12 # Use REGULAR pages
        hemem.INTERNAL_ACTION_PERIOD = self.heMem_EVENTS_THRASHOLD
        sims.append(hemem)

#        self.hemem_1 = HeMemPEBS(int(self.HeMem_INTERVAL / 10), int(self.HeMem_PERIOD / 10))
#        self.hemem_2 = HeMemPEBS(int(self.HeMem_INTERVAL / 100), int(self.HeMem_PERIOD / 100))

        ptscan = PageTableScan(self.pts_SAMPLE_PERIOD, self.pts_SAMPLE_DURATION)
        sims.append(ptscan)

        ptscan_1 = PageTableScan(self.pts_SAMPLE_PERIOD, int(self.pts_SAMPLE_DURATION / 10))
        ptscan_2 = PageTableScan(self.pts_SAMPLE_PERIOD, int(self.pts_SAMPLE_DURATION / 100))
        ptscan_3 = PageTableScan(self.pts_SAMPLE_PERIOD, int(self.pts_SAMPLE_DURATION / 1000))
        ptscan_4 = PageTableScan(self.pts_SAMPLE_PERIOD, int(self.pts_SAMPLE_DURATION / 10000))
        sims.extend([ptscan_1, ptscan_2, ptscan_3, ptscan_4])

        lru = LRU(self.lru_SCAN_PERIOD, self.lru_SWAP_PERIOD)
        #sims.append(lru)

<<<<<<< HEAD
        self.__sim = Simulator([
            self.hemem,
            self.ptscan,
            #self.lru,
=======
        for sim in sims:
            sim.set_internal_action_handler(self.internal_action_handler)
>>>>>>> 78723b8109ac5faa55e4a35ae09f14387bbff94a

        sims.append(self.baseline)
        #sims.append(self)
        self.__sim = Simulator(sims)

    def _calcualte_precision_and_recall(self, sim_hot_pages):
        m = {}
        s = 0
        cn = 0
        for p in self.baseline.pages_by_temp():
            temp = self.baseline.page_temp(p)
            if temp > 0: m[p.va] = temp
            else: cn += 1
            #m[p.va] = temp
            s += temp
        s *= self.HOT_THRESHOLD_RATIO
        hot_threshold = 0
        hot_n = 0
        for va,temp in m.items():
            hot_threshold = temp
            hot_n += 1
            if s <= temp: break
            s -= temp

        base_temp = 0
        i = 0
        for temp in list(m.values())[:len(sim_hot_pages)]:
            i += 1
            self.LOG.debug("temp[%d]: %d", i, temp)
            base_temp += temp

        TP = 0
        FP = 0
        sim_temp = 0

        i = 0
        for p in sim_hot_pages:
            i += 1
            temp = m.pop(p.va, 0)
            self.LOG.debug("sim_temp[%d]: %d", i, temp)
            #temp = m.get(p.va, 0)
            if temp >= hot_threshold: TP += 1
            else:                     FP += 1
            sim_temp += temp

        #self.LOG.info("base_hot_n = %d; threshold = %d; base_temp = %d; sim_temp = %d.", hot_n, hot_threshold, base_temp, sim_temp)

        FN = 0
        TN = 0
        for temp in m.values():
            if temp >= hot_threshold: FN += 1
            else:                     TN += 1

        return ClassifierStats(TP, FP, TN, FN, sim_temp, base_temp, cn)

    def internal_action_handler(self, sim):
        self.update_statistics(sim)

    #def handle_action(self, elapsed):
    #    #self.update_statistics()
    #    pass

    def update_statistics(self, sim):

        cs = self._calcualte_precision_and_recall(sim.hot_pages)

        F1 = Tacc = Accy = Rec = Prec = 'n/a'
        v = cs.Precision()
        if v is not None:
            Prec = f'{v:.2f}'.lstrip('0')
            sim.stat.setdefault('Precision', []).append(v)

        v = cs.Recall()
        if v is not None:
            Rec = f'{v:.2f}'.lstrip('0')
            sim.stat.setdefault('Recall', []).append(v)

        v = cs.Accuracy()
        if v is not None:
            Accy = f'{v:.2f}'.lstrip('0')
            sim.stat.setdefault('Accuracy', []).append(v)

        v = cs.Tacc()
        if v is not None:
            Tacc = f'{v:.2f}'.lstrip('0')
            sim.stat.setdefault('Tacc', []).append(v)

        v = cs.F1Score()
        if v is not None:
            F1 = f'{v:.2f}'.lstrip('0')
            sim.stat.setdefault('F1Score', []).append(v)

        sim.stat.setdefault('TP',[]).append(cs.TP)
        sim.stat.setdefault('FP',[]).append(cs.FP)
        sim.stat.setdefault('TN',[]).append(cs.TN)
        sim.stat.setdefault('FN',[]).append(cs.FN)
        sim.stat.setdefault('CN',[]).append(cs.CN)
        sim.stat.setdefault('Total',[]).append(cs.Total)

        print(f'{sim.name};'
              f' #{cs.Total}(+{cs.CN});'
              f' TP={cs.TP}; TN={cs.TN};'
              f' FP={cs.FP}; FN={cs.FN};'
              f' Precision:{Prec};'
              f' Recall:{Rec};'
              f' Accuracy:{Accy};'
              f' Tacc:{Tacc};'
              f' F1:{F1};')
        return

    def handle_complete(self):
        sims = []
        results = {}
        for sim in [s for s in self.__sim.simulations() if s.name not in ['baseline', 'checker']]: #[self.hemem, self.ptscan]:
            sim.trigger_internal_action()
            for k,v in sim.stat.items():
                s = pd.Series(v)
<<<<<<< HEAD
                print(f"{sim.name}.{k}: 50%={s.quantile(.5):.0f}; m={s.mean():.2f}; σ={s.std():.2f};")
=======
                line = results.setdefault(k, [])
                #print(f"{sim.name}.{k}: 50%={s.quantile(.5):.2f}; m={s.mean():.2f}; σ={s.std():.2f};")
                #print(f"{sim.name}.{k}: {s.quantile(.5):.2f}[{s.mean():.2f}±{s.std():.2f}];")
                if sim.name not in sims:
                    sims.append(sim.name)

                line.append(f"{s.quantile(.5):.2f}[{s.mean():.2f}±{s.std():.2f}]")

        print("values;  " + ";  ".join(sims))
        for key, values in results.items():
            print(f"{key}: {'; '.join(values)}")

>>>>>>> 78723b8109ac5faa55e4a35ae09f14387bbff94a

    def run(self, tracefile):
        rc = self.__sim.run(tracefile)
        if 0 == rc:
            #self.handle_action(0)
            self.handle_complete()
        return rc

#ActionSimulation.LOG.setLevel(DEBUG)
#HeMemPEBS.LOG.setLevel(DEBUG)
#Baseline.LOG.setLevel(DEBUG)
logging.basicConfig(level = INFO)
sim = Accuracy()
exit(sim.run(tracefile))

for t in reader:
    p = t.va / 0x1000
    if p not in uniq_pages:
        uniq_pages[p] = 0

print("memory map populated: ", len(uniq_pages))

#from PIL import Image
#img = Image.new('L', (int(len(uniq_pages)/1024 + 1), 1024), 'white')
#pixels = img.load()

def print_memory_map(m):
    #i = 0
    #for k in sorted(m.keys()):
    #    x = i / 1024
    #    y = i % 1024
    #    v = m[k]
    #    pixels[x,y] = v * 1000 #0#v * 1000000
    #    i += 1

    #img.save("SCREEN", "JPEG")
    ##input("Saved")
    #return

    W = 400
    sz = 0
    line = ""
    os.system('clear')
    print("-" * W)
    legend = " .*-~=>/?+%@"
    for k in sorted(m.keys()):
        v = m[k]
        if v < len(legend):
            line += legend[v]
        else:
            line += "#"
        sz += 1
        if sz == W:
            print(line)
            sz = 0
            line = ""
    print("=" * W)
    input()


#DECAY_last = None
#last = None
#start = None
#for t in reader:
#    if start is None: start = t.time
#    if last is None: last = t.time
#    if DECAY_last is None: DECAY_last = t.time
#
#    if t.time - last > WINDOW:
#        #os.system('clear')
#        print_memory_map(uniq_pages)
#        #print((t.time - start) / 1000000000)
#        last = t.time
#
#    if t.time - DECAY_last > DECAY_window:
#        for k,v in uniq_pages.items():
#            if v > DECAY_value:
#                uniq_pages[k] -= DECAY_value
#            elif v > 0:
#                uniq_pages[k] = 0
#        DECAY_last = t.time
#
#    uniq_pages[t.va / 0x1000] += 1

#exit(0)


#MASK = 0x100000
#
#last = None
#start = None
#for t in reader:
#    if start is None: start = t.time
#
#    if last is None:
#        last = t.time
#        rd = AddressSpace()
#        rd_mib = AddressSpace(MASK)
#
#    if t.time - last > WINDOW:
#        os.system('clear')
#
#        print((t.time - start) / 1000000000, rd.len(), rd_mib.sample_num())
#        rd_mib.flush(600)
#        input()
#        last = t.time
#        rd = AddressSpace()
#        rd_mib = AddressSpace(MASK)
#
#    rec = Record(t.va, t.pa)
#    if t.op == ord('r'):
#        rd.add(rec)
#        rd_mib.add(rec)
