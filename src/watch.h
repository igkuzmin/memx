#pragma once

#include "RDTSC.h"

#include <cstdint>

namespace memx {

class Elapsed {
public:
    Elapsed(const uint64_t tsc = 0) : m_elapsed(tsc) { }

public:
    auto & tsc() { return m_elapsed; }

public:
    uint64_t secs() const  { return TSC_to_sec(m_elapsed); }
    uint64_t msecs() const { return TSC_to_msec(m_elapsed); }
    uint64_t usecs() const { return TSC_to_usec(m_elapsed); }
    uint64_t nsecs() const { return TSC_to_nsec(m_elapsed); }
    uint64_t tsc() const   { return m_elapsed; }

    Elapsed operator+(const Elapsed & rhs) const { return Elapsed(m_elapsed + rhs.m_elapsed); }
    Elapsed & operator+=(const Elapsed & rhs) { m_elapsed += rhs.m_elapsed; return *this; }

private:
    uint64_t m_elapsed;
};

class StopWatch {
    using self = StopWatch;

public:
    self & start()
    {
        m_start = RDTSC();
        return *this;
    }

    self & reset()
    {
        m_elapsed_sum.tsc() = 0;
        m_start = 0;
        return *this;
    }

    Elapsed running() const
    {
        const uint64_t now = RDTSC();
        Elapsed el(now - m_start);
        return el;
    }

    Elapsed stop()
    {
        const Elapsed el = running();
        m_elapsed_sum.tsc() += el.tsc();
        m_start = 0;
        return el;
    }

    auto secs() const  { return m_elapsed_sum.secs(); }
    auto msecs() const { return m_elapsed_sum.msecs(); }
    auto usecs() const { return m_elapsed_sum.usecs(); }
    auto nsecs() const { return m_elapsed_sum.nsecs(); }

private:
    uint64_t m_start {RDTSC()};
    Elapsed m_elapsed_sum;
};

}
