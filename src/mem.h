#pragma once

#include "forward.h"

#include "watch.h"
#include "bench_util.h"

#include <bitset>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <random>

namespace memx {


using nodemask_t = std::bitset<64>;

class MemRegion
{
public:
    static const std::size_t PAGE_SIZE;

public:
    MemRegion(void * ptr, const std::size_t sz)
      : m_ptr(reinterpret_cast<uint8_t*>(ptr))
      , m_size(sz)
    {}

    ~MemRegion();

public:
    void load_all_pages();

public:
    // offset API

    template<typename T> T read(const std::size_t off)
    { assert(off <= m_size); return MemRegion::read<T>(m_ptr + off); }

    template<typename T> void write(const std::size_t off)
    { assert(off <= m_size); return MemRegion::write<T>(m_ptr + off); }

    template<typename T> void write_nt(const std::size_t off)
    { assert(off <= m_size); return MemRegion::write_nt<T>(m_ptr + off); }

    template<typename T> void clflush(const std::size_t off)
    { assert(off <= m_size); return MemRegion::clflush<sizeof(T)>(m_ptr + off); }

    template<size_t SZ = 64> void clflush(const std::size_t off)
    { assert(off <= m_size); return MemRegion::clflush_ptr<SZ>(m_ptr + off); }

    void clflush(const std::size_t off, const std::size_t sz)
    { assert(off + sz <= m_size); return MemRegion::clflush_ptr(m_ptr + off, sz); }

    static void mfence();

    //template<typename T> void clflushopt(const std::size_t off)
    //{ assert(off <= m_size); return MemRegion::clflushopt<sizeof(T)>(m_ptr + off); }


    bool bind(const nodemask_t msk, const std::size_t sz = 0, const std::size_t off = 0)
    { assert(sz + off <= m_size); return MemRegion::bind(m_ptr + off, sz != 0 ? sz : m_size, msk); }

    Elapsed measure_read(std::size_t offset, uint16_t bytes) const;
    Elapsed measure_write(std::size_t offset, uint16_t bytes) const;
    Elapsed measure_nope(std::size_t offset, uint16_t bytes) const;

    Elapsed measure_read_clflush(std::size_t offset, uint16_t bytes) const;
    Elapsed measure_write_mfence(std::size_t offset, uint16_t bytes) const;
    Elapsed measure_write_nt(std::size_t offset, uint16_t bytes) const;

public:
    // pointer API
    static void load_pages(void *, std::size_t);
    static bool bind(void *, std::size_t, nodemask_t);

    template<typename T> static void read(void * ptr) {
        do_not_optimize_away(* reinterpret_cast<volatile T *>(ptr));
    }

    template<typename T> static void write(void * ptr);
    template<typename T> static void write_nt(void * ptr);

    template<size_t SZ> static void clflush_ptr(void * ptr);
    static void clflush_ptr(void * ptr, std::size_t sz);
    //template<size_t SZ> static void clflushopt(void * ptr);

    const auto * get_pointer() const { return m_ptr; }
    const auto get_size() const { return m_size; }

private:
    uint8_t * const m_ptr;
    std::size_t m_size;
};

class Mem
{

public:
    Mem();
    ~Mem();
    Mem(const Config &);
    Mem & setup(const Config &);
    // Options

public:
    MemRegion allocate(std::size_t) const;

private:
    void * mmap(std::size_t) const;

private:
    bool m_bind : 1;
    bool m_load : 1;
    bool m_read : 1;
    bool m_write : 1;

    unsigned m_node;
    double   m_ratio;
    int      m_fd;
};

}

#include "mem.inl"
