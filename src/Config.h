#pragma once

#include <chrono>
#include <cstddef>
#include <cstdint>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

namespace memx {

enum class WriteMode {empty, nt, clwb, mfence};
enum class ReadMode {empty, clflush};

class Config
{
    using self = Config;
public:
    auto get_seed() const { return m_seed; }
    auto get_working_set_size() const { return m_working_set_size; }
    auto get_limit_seconds() const { return m_limit_seconds; }
    auto get_read_ratio() const { return m_read_ratio; }
    auto get_num_threads() const { return m_num_threads; }
    auto get_limit_operations() const { return m_limit_operations; }
    auto get_stride() const { return m_stride; }
    auto & get_access_pattern() const { return m_access_pattern; }
    auto & get_access_log() const { return m_access_log; }
    auto & get_log_level() const { return m_log_level; }
    auto & get_pmap_file() const { return m_pmap_file; }
    auto & get_touch_bytes() const { return m_touch_bytes; }
    auto & get_alignment() const { return m_alignment; }
    auto & get_write_mode() const { return m_write_mode; }
    auto & get_read_mode() const { return m_read_mode; }
    auto & get_remote_node() const { return m_remote_node; }
    auto & get_remote_ratio() const { return m_remote_ratio; }
    auto & get_remote_file() const { return m_remote_file; }
    auto & get_bind() const { return m_bind; }
    auto & get_mean() const { return m_mean; }
    auto & get_stddev() const { return m_stddev; }

    auto & get_mean_2() const { return m_mean_2; }
    auto & get_stddev_2() const { return m_stddev_2; }
    auto & get_read_ratio_2() const { return m_read_ratio_2; }

    auto get_clfence_after_op() const { return m_clflush_after_op; }

public:
    self & set_working_set_size(const std::size_t v) { m_working_set_size = v; return *this; }
    self & set_access_pattern(const std::string_view v) { m_access_pattern = v; return *this; }
    self & set_access_log(const std::string_view v) { m_access_log = v; return *this; }
    self & set_read_ratio(const std::size_t v) { m_read_ratio = v; return *this; }
    self & set_seed(const uint64_t v) { m_seed = v; return *this; }
    self & set_log_level(const std::string_view v) { m_log_level = v; return *this; }
    self & set_pmap_file(const std::string_view v) { m_pmap_file = v; return *this; }
    self & set_limit_seconds(const std::chrono::seconds & v) { m_limit_seconds = v; return *this; }
    self & set_limit_operations(const uint64_t v) { m_limit_operations = v; return *this; }
    self & set_num_threads(const uint16_t v) { m_num_threads = v; return *this; }
    self & set_touch_bytes(const uint16_t v) { m_touch_bytes = v; return *this; }
    self & set_alignment(const uint16_t v) { m_alignment = v; return *this; }
    self & set_write_mode(const std::string_view v) { m_write_mode = v; return *this; }
    self & set_read_mode(const std::string_view v) { m_read_mode = v; return *this; }
    self & set_stride(const std::size_t v) { m_stride = v; return *this; }
    self & set_remote_node(const std::optional<uint8_t> v) { m_remote_node = v; return *this; }
    self & set_remote_ratio(const double v) { m_remote_ratio = v; return *this; }
    self & set_remote_file(const std::string & v) { m_remote_file = v; return *this; }
    self & set_bind(const std::string v) { m_bind = v; return *this; }
    self & set_mean(const double v) { m_mean = v; return *this; }
    self & set_stddev(const double v) { m_stddev = v; return *this; }
    self & set_mean_2(const double v) { m_mean_2 = v; return *this; }
    self & set_stddev_2(const double v) { m_stddev_2 = v; return *this; }
    self & set_read_ratio_2(const std::size_t v) { m_read_ratio_2 = v; return *this; }
    self & set_clflush_after_op(const bool v = true) { m_clflush_after_op = v; return *this; }

public:
    static std::vector<Config> parse(const std::string & json);
    static bool parse_args(int argc, char ** argv, Config &);

private:
    std::optional<uint16_t> m_num_threads;

    uint64_t m_seed {0};
    std::size_t m_working_set_size {0};
    unsigned m_read_ratio {100}; // 100 - read/only
    std::string m_access_pattern {"uniform"};
    std::string m_log_level {"debug"};
    std::string m_write_mode;
    std::string m_read_mode;
    std::string m_remote_file;
    std::string m_access_log;
    std::string m_pmap_file;
    std::chrono::seconds m_limit_seconds {10};
    uint64_t m_limit_operations {10'000'000'000};
    uint16_t m_touch_bytes {8};
    uint16_t m_alignment {8};
    std::size_t m_stride {4*1024};
    std::string m_bind {"kmod"};

    std::optional<uint8_t> m_remote_node;
    double m_remote_ratio {1};

    bool m_clflush_after_op {false};

    std::optional<double> m_mean;
    std::optional<double> m_stddev;

    std::optional<double> m_mean_2;
    std::optional<double> m_stddev_2;
    std::optional<unsigned> m_read_ratio_2;
};


WriteMode parse_write_mode(const std::string & op);
ReadMode parse_read_mode(std::string_view);

}
