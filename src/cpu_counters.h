#pragma once
#include <memory>
namespace memx {

class CPUCounters
{
    class Impl;

public:
    CPUCounters();
    ~CPUCounters();
    static bool is_available();

private:
    std::unique_ptr<Impl> m_impl;
};

}
