#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <limits>
#include <vector>

namespace memx {

struct Value {
    Value() = default;
    Value(const uint64_t v, const size_t n = 1) : value(v), count(n) {}
    bool operator<(const Value & o) const { return value < o.value; }

    uint64_t value = 0;
    size_t count = 0;
};

struct DoubleValue {
    DoubleValue() = default;
    DoubleValue(const double v, const size_t n = 1) : value(v), count(n) {}
    bool operator<(const Value & o) const { return value < o.value; }

    double value = 0;
    size_t count = 0;
};

class Histogram
{
    using self = Histogram;
public:
    /**
     * @param capacity      compress values, when reach capacity
     */
    Histogram(const size_t capacity = 4000) { m_values.reserve(capacity); }

    /**
     * @param v     value to be added to the sampling set
     */
    self & add_sample(uint64_t);
    self & add_sample(uint64_t, size_t);
    void merge(const Histogram &);

    uint64_t get_min() const;
    uint64_t get_max() const;
    auto get_num() const { return m_number; }
    //const auto get_50p() const { return m_50p; }
    //const auto get_99p() const { return m_99p; }

    double get_mean() const;
    double get_variance() const;
    double get_deviation() const { return std::sqrt(get_variance()); }
    uint64_t get_percentile(double) const;
    uint64_t get_median() const { return get_percentile(50); }

    /**
     * Produces probability density function
     *
     * @param m     number of buckets
     */
    std::vector<DoubleValue> get_density_average(size_t m) const;
    std::vector<DoubleValue> get_density_median(size_t m) const;
    const std::vector<Value> & get_density() const { return m_values; }

    /**
     * Produces probability distribution among specified number of buckets;
     *
     * @param m     number of buckets
     */
    std::vector<double> get_distribution(size_t m = 10) const;

public:
    // Public for TEST purposes
    //void compress_values(size_t m = 0);
    //const auto & get_values() const { return m_values; }
private:
    size_t m_number = 0;
    std::vector<Value> m_values;
};

}
