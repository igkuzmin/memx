#pragma once

#include "Config.h"

#include <ctime>
#include <exception>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <mutex>

#define THROW_ERROR(msg) { \
    std::ostringstream oss; \
    oss << __FILE__ << ':' <<__LINE__ << ": " << msg; \
    throw std::runtime_error(oss.str()); \
}

#define __PUT_LOG_RECORD(lvl, msg) \
    std::time_t __time = std::time(nullptr); \
    std::tm __tm = *std::localtime(&__time); \
    auto lk = logging_lock(); \
    std::cout << std::put_time(&__tm, "%d %b %H:%M:%S ") << __FILE__ << ':' <<__LINE__ << ": " lvl " " << msg << "\n";

#ifdef DEBUG
#  define DDUMP(msg) { \
      if (logging_level() >= LogLevel::Debug) { \
          __PUT_LOG_RECORD("<dump>", msg) \
      } \
}
#else
#   define DDUMP(msg)
#endif

#define DEBUGLOG(msg) { \
    if (logging_level() >= LogLevel::Debug) { \
        __PUT_LOG_RECORD("<debug>", msg) \
    } \
}

#define STATUSLOG(msg) { \
    if (logging_level() >= LogLevel::Status) { \
        __PUT_LOG_RECORD("<status>", msg) \
    } \
}

#define WARNING(msg) { \
    if (logging_level() >= LogLevel::Warning) { \
        __PUT_LOG_RECORD("<warning>", msg) \
    } \
}

namespace memx {

enum class LogLevel { Silent = 0, Warning, Status, Debug, Dump };

LogLevel logging_level();

std::lock_guard<std::mutex> logging_lock();

void logging_level_set(const Config &);
void logging_level_increase();
void logging_level_decrease();

}
