#include "Config.h"
#include "log.h"
#include "cpu_counters.h"

#include <getopt.h>
#include <json/json.h>

#include <cstdlib>
#include <chrono>
#include <fstream>
#include <set>

namespace memx {

WriteMode parse_write_mode(const std::string & op)
{
    if (op == "mfence") {
        return WriteMode::mfence;
    }
    if (op == "clwb") {
        return WriteMode::clwb;
    }
    if (op == "nt") {
        return WriteMode::nt;
    }
    if (op.empty() || op == "empty") {
        return WriteMode::empty;
    }
    THROW_ERROR("Unknown write operation '"<< op << "'");
}

ReadMode parse_read_mode(const std::string_view op)
{
    if (op == "clflush") {
        return ReadMode::clflush;
    }
    if (op.empty() || op == "empty") {
        return ReadMode::empty;
    }
    THROW_ERROR("Unknown read operation '"<< op << "'");
}

std::vector<Config> Config::parse(const std::string & json_file)
{
    std::ifstream ifs(json_file);
    if (!ifs) THROW_ERROR("Can't open file '" << json_file << "'");

    Json::Value json;
    Json::Reader reader;
    if (!reader.parse(ifs, json))
        THROW_ERROR("Can't parse file '" << json_file << "'"
                ": " << reader.getFormattedErrorMessages());

    Config cfg;
    for(Json::ValueIterator i = json.begin(); i != json.end(); ++i) {
        if (i->isArray() || i->isObject()) {
            continue;
        }
        const std::string key = i.key().asString();
        if (key == "seed") {
            if (!i->isNumeric()) THROW_ERROR("Expect numeric seed :" << *i);
            cfg.set_seed(i->asUInt64());
        }
        else if (key == "limit_seconds") {
            if (!i->isUInt()) THROW_ERROR("Expect numeric limit_seconds :" << *i);
            cfg.set_limit_seconds(std::chrono::seconds(i->asUInt64()));
        }
        else if (key == "touch_bytes") {
            if (!i->isUInt()) THROW_ERROR("Expect numeric touch_bytes :" << *i);
            cfg.set_touch_bytes(i->asUInt64());
        }
        else if (key == "alignment") {
            if (!i->isUInt()) THROW_ERROR("Expect numeric alignment :" << *i);
            cfg.set_alignment(i->asUInt64());
        }
        else if (key == "log_level") {
            if (!i->isString()) THROW_ERROR("Expect string log_level :" << *i);
            static const std::set<std::string> levels = { "debug", "warning", "status", "silent"};
            const std::string level = i->asString();
            if (levels.find(level) == levels.end()) THROW_ERROR("Unknown log_level :" << level);
            cfg.set_log_level(level);
        }
        else if (key == "remote_file") {
            if (!i->isString()) THROW_ERROR("Expect string remote_file :" << *i);
            cfg.set_remote_file(i->asString());
        }
        else if (key == "remote_ratio") {
            if (!i->isDouble()) THROW_ERROR("Expect a real remote_ratio :" << *i);
            cfg.set_remote_ratio(i->asDouble());
        }
        else if (key == "mean") {
            if (!i->isDouble()) THROW_ERROR("Expect a real mean :" << *i);
            const auto v = i->asDouble();
            if (v < 0 || v > 1) THROW_ERROR("Expect mean in the [0,1] :" << *i);
            cfg.set_mean(v);
        }
        else if (key == "stddev") {
            if (!i->isDouble()) THROW_ERROR("Expect a real stddev :" << *i);
            const auto v = i->asDouble();
            if (v < 0 || v > 1) THROW_ERROR("Expect deviation in the [0,1] :" << *i);
            cfg.set_stddev(v);
        }
        else if (key == "clflush_after_op") {
            if (!i->isBool()) THROW_ERROR("Expect a bool clflush_after_op :" << *i);
            cfg.set_clflush_after_op(i->asBool());
        }
        else {
            DEBUGLOG(i.key() << "--" << *i);
        }
    }
    //for (const Json::Value & key: json) {
    //    std::cout << key << std::endl;
    //}

    return {};
}

static struct option s_long_options[] =
{
    /* These options set a flag. */
    {"alignment",        required_argument, 0, 'a'},
    {"verbose",          no_argument,       0, 'v'},
    {"silent",           no_argument,       0, 's'},
    {"working_set",      required_argument, 0, 'w'},
    {"read_ratio",       required_argument, 0, 'r'},
    {"access_pattern",   required_argument, 0, 'p'},
    {"write_mode",       required_argument, 0, 'm'},
    {"touch_bytes",      required_argument, 0, 'b'},
    {"num_threads",      required_argument, 0, 't'},
    {"remote_node",      required_argument, 0, 'n'},
    {"remote_file",      required_argument, 0, 'f'},
    {"access_log",       required_argument, 0, 0},
    {"pmap_file",        required_argument, 0, 0},
    {"read_mode",        required_argument, 0, 0},
    {"seed",             required_argument, 0, 0},
    {"log_level",        required_argument, 0, 0},
    {"limit_seconds",    required_argument, 0, 0},
    {"limit_operations", required_argument, 0, 0},
    {"stride",           required_argument, 0, 0},
    {"remote_ratio",     required_argument, 0, 0},
    {"no-bind",          no_argument,       0, 0},
    {"clflush_after_op", no_argument,       0, 0},
    {"bind",             required_argument, 0, 0},
    {"mean",             required_argument, 0, 0},
    {"stddev",           required_argument, 0, 0},
    {"mean_2",           required_argument, 0, 0},
    {"stddev_2",         required_argument, 0, 0},
    {"read_ratio_2",     required_argument, 0, 0},
    {"help",             no_argument,       0, 0},
    {0, 0, 0, 0}
};

void exit_help()
{

    std::cout <<
        "memx - yet another memory benchmark tool\n"
        "\n"
        "SYNOPSYS\n"
        "    memx [OPTIONS]\n"
        "\n"
        "      PCM is" << (CPUCounters::is_available() ? "" : " not") << " available\n"
        "\n"
        "OPTIONS\n";
    for (const auto & o: s_long_options) {
        if (o.name == 0) continue;
        std::string_view name = o.name;
        std::cout << "    --" << name;
        if (o.val) std::cout << ", -" << static_cast<char>(o.val);
        if (o.has_arg) std::cout << " <arg>";
        std::cout << "\n";
    }
    exit(0);
}

bool Config::parse_args(const int argc, char ** argv, Config & cfg)
{
    for(;;)  {
         int idx = 0;
         const int c = getopt_long(argc, argv, "vsw:r:p:m:a:b:t:n:f:", s_long_options, &idx);

         /* Detect the end of the options. */
         if (c == -1) { break; }
         switch (c) {
         case 'a': cfg.set_alignment(std::atol(optarg)); break;
         case 'b': cfg.set_touch_bytes(std::atol(optarg)); break;
         case 'm': cfg.set_write_mode(optarg); break;
         case 'n': cfg.set_remote_node(std::atol(optarg)); break;
         case 'f': cfg.set_remote_file(optarg); break;
         case 'p': cfg.set_access_pattern(optarg); break;
         case 'r': cfg.set_read_ratio(std::atol(optarg)); break;
         case 's': cfg.set_log_level(
                     cfg.get_log_level() == "dump" ? "debug":
                     cfg.get_log_level() == "debug" ? "status": "silent"); break;
         case 't': cfg.set_num_threads(std::atol(optarg)); break;
         case 'v': cfg.set_log_level(
                     cfg.get_log_level() == "silent" ? "status":
                     cfg.get_log_level() == "status" ? "debug": "dump"); break;
         case 'w': cfg.set_working_set_size(1024ll*1024* std::atol(optarg)); break;
         case '?':
             /* getopt_long already printed an error message. */
             exit_help();

         case 0: {
             const std::string name = s_long_options[idx].name;
             if (name == "help")
                 exit_help();
             else if (name == "limit_operations")
                 cfg.set_limit_operations(std::atol(optarg));
             else if (name == "access_log")
                 cfg.set_access_log(optarg);
             else if (name == "pmap_file")
                 cfg.set_pmap_file(optarg);
             else if (name == "limit_seconds")
                 cfg.set_limit_seconds(std::chrono::seconds(std::atol(optarg)));
             else if (name == "log_level")
                 cfg.set_log_level(optarg);
             else if (name == "no-bind")
                 cfg.set_bind("");
             else if (name == "bind")
                 cfg.set_bind(optarg);
             else if (name == "remote_ratio") {
                 auto ratio = std::atof(optarg);
                 if (ratio > 1 || ratio < 0) {
                     THROW_ERROR("remote_ratio should belong [0,1] interval");
                 }
                 cfg.set_remote_ratio(ratio);
             }
             else if (name == "mean") {
                 auto v = std::atof(optarg);
                 if (v < 0 || v > 1) THROW_ERROR("mean should belong [0,1]");
                 cfg.set_mean(v);
             }
             else if (name == "stddev") {
                 auto v = std::atof(optarg);
                 if (v < 0 || v > 1) THROW_ERROR("stddev should belong [0,1]");
                 cfg.set_stddev(v);
             }
             else if (name == "mean_2") {
                 auto v = std::atof(optarg);
                 if (v < 0 || v > 1) THROW_ERROR("mean should belong [0,1]");
                 cfg.set_mean_2(v);
             }
             else if (name == "stddev_2") {
                 auto v = std::atof(optarg);
                 if (v < 0 || v > 1) THROW_ERROR("stddev should belong [0,1]");
                 cfg.set_stddev_2(v);
             }
             else if (name == "read_ratio_2")
                cfg.set_read_ratio_2(std::atol(optarg));
             else if (name == "read_mode")
                cfg.set_read_mode(optarg);
             else if (name == "seed")
                 cfg.set_seed(std::atol(optarg));
             else if (name == "clflush_after_op")
                 cfg.set_clflush_after_op();

             else THROW_ERROR("Unknown option: " << name);
         } break;
         default:
            THROW_ERROR("Unknown option '" << c << "'");
         }
     }
     ///* Print any remaining command line arguments (not options). */
     if (optind < argc) {
         THROW_ERROR("Unrecognized command line arguments left");
     //    while (optind < argc) {

     //    }
     }
     return true;
}

} // namespace memx
