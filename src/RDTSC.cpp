#include "RDTSC.h"

#include <cstdint>
#include <thread>

namespace memx {

static uint64_t _rdtsc_rate;
static double _rdtsc_rate_nsec;

uint64_t TSC_init_rate()
{
    if (!_rdtsc_rate) {
        const uint64_t tsc = RDTSC();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        _rdtsc_rate = RDTSC() - tsc;

        if (!_rdtsc_rate) {
            _rdtsc_rate = 1; // for unsupported platforms
        }
        _rdtsc_rate_nsec = (double)(_rdtsc_rate) / 1000000;
    }
    return _rdtsc_rate;
}

uint64_t TSC_to_msec(uint64_t tsc)
{
    if (!_rdtsc_rate) {
        TSC_init_rate();
    }
    return tsc / (_rdtsc_rate / 1000);
}

uint64_t TSC_to_usec(uint64_t tsc)
{
    if (!_rdtsc_rate) {
        TSC_init_rate();
    }
    // drop high 10 bit to avoid operation overflow
    return ((tsc & 0x3FFFFFFFFFFFFFULL) * 1000) / (_rdtsc_rate / 1000);
}

uint64_t TSC_to_nsec(uint64_t tsc)
{
    if (!_rdtsc_rate) {
        TSC_init_rate();
    }
    // drop high 20 bit to avoid operation overflow
    return ((tsc & 0xFFFFFFFFFFFULL) * 1000000) / (_rdtsc_rate / 1000);
}

double TSC_to_sec(uint64_t tsc)
{
    if (!_rdtsc_rate) {
        TSC_init_rate();
    }
    return (double)(tsc) / _rdtsc_rate;
}

uint64_t usec_to_TSC(uint64_t usec)
{
    if (!_rdtsc_rate) {
        TSC_init_rate();
    }

    return (usec * (_rdtsc_rate / 1000)) / 1000;
}

} // memx
