#include "access_pattern.h"
#include "Config.h"
#include "log.h"

#include <cassert>
#include <memory>

namespace memx {

class BaseAccessPattern : public IAccessPattern
{
public:
    BaseAccessPattern(
            generator_t & generator,
            const unsigned rw_ratio)
      : m_generator(generator)
      , m_read_ratio(rw_ratio)
      , m_op_distribution(0, 100)
    {}

public:
    Operation next_operation() final {
        const auto v = m_op_distribution(m_generator);
        if (v < m_read_ratio) {
            return Operation::Read;
        }
        return Operation::Write;
    }

    void flush_description(std::ostream & out) override {
        out << "rr=" << m_read_ratio << "%";
    }

protected:
    generator_t & m_generator;
    const unsigned m_read_ratio;
    std::uniform_int_distribution<uint8_t> m_op_distribution;
};

class UnifromPattern : public BaseAccessPattern
{
    using base = BaseAccessPattern;
public:
    UnifromPattern(
            generator_t & generator,
            std::size_t range,
            unsigned rw_ratio,
            uint16_t alignment)
      : BaseAccessPattern(generator, rw_ratio)
      , m_alignment(alignment)
      , m_distribution(0, range)
    {
    }

    uint64_t next_offset() final {
        uint64_t res = m_distribution(m_generator);
        return res / m_alignment * m_alignment;
    }

    void flush_description(std::ostream & oss) final {
        oss << "uniform(";
        base::flush_description(oss);
        oss << ")";
    }


private:
    const uint16_t m_alignment;
    std::uniform_int_distribution<uint64_t> m_distribution;
};

class LinearPattern : public BaseAccessPattern
{
    using base = BaseAccessPattern;
public:
    LinearPattern(
            generator_t & generator,
            const std::size_t range,
            const unsigned rw_ratio,
            const uint16_t alignment,
            const std::size_t stride)
      : BaseAccessPattern(generator, rw_ratio)
      , m_alignment(alignment)
      , m_stride(stride)
      , m_range(range)
    {
        std::uniform_int_distribution<std::size_t> distribution(0, range);
        m_last_offset = distribution(generator) / alignment * alignment;
    }

    void flush_description(std::ostream & oss) final {
        oss << "linear(stride = " << m_stride << "; ";
        base::flush_description(oss);
        oss << ")";
    }

    uint64_t next_offset() final
    {
        const auto next =
            ((m_last_offset + m_stride) % m_range) / m_alignment * m_alignment;

        if (next == m_last_offset) {
            m_last_offset = ((m_last_offset + m_alignment) % m_range);
        }
        else {
            m_last_offset = next;
        }

        return m_last_offset;
    }

private:
    const uint16_t m_alignment;
    const std::size_t m_stride;
    const std::size_t m_range;
    std::size_t m_last_offset;
};

class UnimodalNormalPattern : public BaseAccessPattern
{
    using base = BaseAccessPattern;
public:
    UnimodalNormalPattern(
            generator_t & generator,
            const std::size_t range,
            const double mean,
            const double stddev,
            const unsigned rw_ratio,
            const uint16_t alignment)
      : BaseAccessPattern(generator, rw_ratio)
      , m_alignment(alignment)
      , m_range(range)
      , m_distribution(mean, stddev)
      //, m_distribution(
      //        static_cast<double>(mean) / range,
      //        static_cast<double>(stddev) / range)
    {
    }

    uint64_t next_offset() final {
        const auto res = m_distribution(m_generator);
        const auto s1 = std::lround(res * m_range) % m_range;
        const auto s2 = s1 / m_alignment * m_alignment;
        //std::cout << "res: " << res << "; round: " << s1 << "; alligned: " << s2 << "\n";
        return s2;
    }

    void flush_description(std::ostream & oss) final {
        oss << "unimodal("
            "μ=" << m_distribution.mean() << "; "
            "σ=" << m_distribution.stddev() << "; ";
        base::flush_description(oss);
        oss << ")";
    }

private:
    const uint16_t m_alignment;
    const std::size_t m_range;
    std::normal_distribution<double> m_distribution;
};

class CombinedPattern : public IAccessPattern
{
    uint64_t next_offset() final {
        next_pattern();
        return current_pattern().next_offset();
    }

    Operation next_operation() final {
        return current_pattern().next_operation();
    }

    void next_pattern() {
        m_current_pattern = m_pattern_dist(m_generator);
    }

    IAccessPattern & current_pattern() {
        return *m_patterns[m_current_pattern];
    }

    virtual void flush_description(std::ostream & oss) final {
        oss << "[";
        size_t n = 0;
        for (const auto & p: m_patterns) {
            p->flush_description(oss);
            if (++n < m_patterns.size()) oss << " ";
        }
        oss << "]";
    }

public:
    CombinedPattern(
            generator_t & generator,
            std::vector<std::unique_ptr<IAccessPattern>> && l)
      : m_generator(generator)
      , m_pattern_dist(0, l.size() - 1)
      , m_patterns(std::move(l))
    {
        assert(m_patterns.size() > 1 && "Empty pattern set is not allowed");
    }

private:
    generator_t & m_generator;
    std::uniform_int_distribution<uint8_t> m_pattern_dist;
    std::vector<std::unique_ptr<IAccessPattern>> m_patterns;
    size_t m_current_pattern {0};
};


IAccessPattern::uptr_t IAccessPattern::create(
        const Config & cfg,
        generator_t & generator)
{
    if (cfg.get_working_set_size() == 0) {
        THROW_ERROR("Working set size is a mandatory property");
    }

    if (cfg.get_access_pattern() == "uniform") {
        return std::make_unique<UnifromPattern>(
                generator,
                cfg.get_working_set_size() - cfg.get_touch_bytes(),
                cfg.get_read_ratio(),
                cfg.get_alignment());
    }

    if (cfg.get_access_pattern() == "linear") {
        return std::make_unique<LinearPattern>(
                generator,
                cfg.get_working_set_size() - cfg.get_touch_bytes(),
                cfg.get_read_ratio(),
                cfg.get_alignment(),
                cfg.get_stride());
    }

    if (cfg.get_access_pattern() == "unimodal") {
        return std::make_unique<UnimodalNormalPattern>(
                generator,
                cfg.get_working_set_size() - cfg.get_touch_bytes(),
                cfg.get_mean().value_or(.5),
                cfg.get_stddev().value_or(.17),
                cfg.get_read_ratio(),
                cfg.get_alignment());
    }

    if (cfg.get_access_pattern() == "bimodal") {
        std::vector<std::unique_ptr<IAccessPattern>> patterns;
        patterns.emplace_back(
            std::make_unique<UnimodalNormalPattern>(
                generator,
                cfg.get_working_set_size() - cfg.get_touch_bytes(),
                cfg.get_mean().value_or(.3),
                cfg.get_stddev().value_or(.17),
                cfg.get_read_ratio(),
                cfg.get_alignment())
            );
        patterns.emplace_back(
            std::make_unique<UnimodalNormalPattern>(
                generator,
                cfg.get_working_set_size() - cfg.get_touch_bytes(),

                cfg.get_mean_2().value_or(
                    cfg.get_mean().value_or(.6)),

                cfg.get_stddev_2().value_or(
                    cfg.get_stddev().value_or(.17)),

                cfg.get_read_ratio_2().value_or(0),

                cfg.get_alignment())
            );
        return std::make_unique<CombinedPattern>(generator, std::move(patterns));
    }

    THROW_ERROR("Unknown access pattern '"<< cfg.get_access_pattern() << "'");
}

} // namespace memx
