#include "log.h"


namespace memx {

static LogLevel s_level = LogLevel::Debug;

LogLevel logging_level()
{
    return s_level;
}

void logging_level_set(const Config & cfg)
{
    const auto lvl = cfg.get_log_level();
    if (lvl == "dump") {
        s_level = LogLevel::Dump;
    }
    else if (lvl == "debug") {
        s_level = LogLevel::Debug;
    }
    else if (lvl == "status") {
        s_level = LogLevel::Status;
    }
    else if (lvl == "warning") {
        s_level = LogLevel::Warning;
    }
    else if (lvl == "silent") {
        s_level = LogLevel::Silent;
    }
    else {
        THROW_ERROR("Unknown log level: " << lvl);
    }
}

void logging_level_increase()
{
    const auto lvl = static_cast<int>(s_level);
    if (lvl < static_cast<int>(LogLevel::Dump)) {
        s_level = static_cast<LogLevel>(lvl + 1);
    }
}

void logging_level_decrease()
{
    const auto lvl = static_cast<int>(s_level);
    if (lvl > 0) {
        s_level = static_cast<LogLevel>(lvl - 1);
    }
}

std::lock_guard<std::mutex> logging_lock()
{
    static std::mutex s_logging_lock;
    return std::lock_guard<std::mutex>(s_logging_lock);
}


}
