#include <immintrin.h>

namespace memx {

template<typename T> struct Empty;
template<> struct Empty<uint64_t> { static constexpr uint64_t value = 0xCafeBabeCafeBabe; };
template<> struct Empty<uint32_t> { static constexpr uint32_t value = 0xCafeBabe; };
template<> struct Empty<uint16_t> { static constexpr uint32_t value = 0xBabe; };
template<> struct Empty<uint8_t>  { static constexpr uint32_t value = 0xBe; };

template<size_t SZ> void MemRegion::clflush_ptr(void * ptr)
{
    _mm_clflush(ptr);
    if constexpr (SZ > 64) {
        MemRegion::clflush_ptr<SZ - 64>(static_cast<uint8_t *>(ptr) + 64);
    }
}

inline void MemRegion::mfence()
{ _mm_mfence(); }


//template<size_t SZ> void MemRegion::clflushopt(void * ptr)
//{
//    _mm_clflushopt(ptr);
//    if constexpr (SZ > 8) {
//        MemRegion::clflushopt<SZ - 8>(ptr + 8);
//    }
//}

template<typename T> void MemRegion::write(void * ptr)
{
    *(reinterpret_cast<volatile T *>(ptr)) = Empty<T>::value;
}

template<> inline void MemRegion::write_nt<__m64>(void * ptr)
{
    _mm_stream_pi(static_cast<__m64*>(ptr), __m64 {
            static_cast<int>(0xCafeBabe),
            static_cast<int>(0xCafeBabe),
        });
}

template<> inline void MemRegion::write_nt<__m128>(void * ptr)
{
    _mm_stream_ps(static_cast<float*>(ptr), __m128 {
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
    });
}

template<> inline void MemRegion::write_nt<__m256>(void * ptr)
{
    _mm256_stream_ps(static_cast<float*>(ptr), __m256 {
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
        });
}

template<> inline void MemRegion::write_nt<__m512>(void * ptr)
{
#ifdef __AVX512F__    // should be __AVX512VL__
    _mm512_stream_ps(static_cast<float*>(ptr), __m512 {
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
            static_cast<float>(0xCafeBabe),
        });
#else
    MemRegion::write_nt<__m256>(ptr);
    MemRegion::write_nt<__m256>(static_cast<uint8_t*>(ptr) + 32);
#endif
}

}
