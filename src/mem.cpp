#include "Config.h"
#include "bench_util.h"
#include "log.h"
#include "mem.h"
#include "watch.h"

#include <numa.h>
#include <numaif.h>
#include <omp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <functional>

#include <byteswap.h>

namespace memx {

const std::size_t MemRegion::PAGE_SIZE = sysconf(_SC_PAGE_SIZE);

//template<typename T> Elapsed do_measure_read(void * ptr) {
//    StopWatch sw;
//    do_not_optimize_away(MemRegion::read<T>(ptr));
//    return sw.stop();
//}


template<const uint16_t SZ> __attribute__((noinline))
    void read_t(uint8_t * ptr)
{
    if constexpr (SZ > 8) {
        MemRegion::read<uint64_t>(ptr);
        read_t<SZ - 8>(ptr + 8);
    }
    else if constexpr (SZ == 8) {
        MemRegion::read<uint64_t>(ptr);
    }
    else if constexpr (SZ == 7) {
        MemRegion::read<uint32_t>(ptr);
        MemRegion::read<uint16_t>(ptr + 4);
        MemRegion::read<uint8_t>(ptr + 6);
    }
    else if constexpr (SZ == 6) {
        MemRegion::read<uint32_t>(ptr);
        MemRegion::read<uint16_t>(ptr + 4);
    }
    else if constexpr (SZ == 5) {
        MemRegion::read<uint32_t>(ptr);
        MemRegion::read<uint8_t>(ptr + 4);
    }
    else if constexpr (SZ == 4) {
        MemRegion::read<uint32_t>(ptr);
    }
    else if constexpr (SZ == 3) {
        MemRegion::read<uint16_t>(ptr);
        MemRegion::read<uint8_t>(ptr + 2);
    }
    else if constexpr (SZ == 2) {
        MemRegion::read<uint32_t>(ptr);
    }
    else if constexpr (SZ == 1) {
        MemRegion::read<uint32_t>(ptr);
    }
    else static_assert(SZ != 0);
}

template<const uint16_t SZ> void write_t(uint8_t * const ptr)
{
    if constexpr (SZ > 8) {
        MemRegion::write<uint64_t>(ptr);
        write_t<SZ - 8>(ptr + 8);
    }
    else if constexpr (SZ == 8) {
        MemRegion::write<uint64_t>(ptr);
    }
    else if constexpr (SZ == 7) {
        MemRegion::write<uint32_t>(ptr);
        MemRegion::write<uint16_t>(ptr + 4);
        MemRegion::write<uint8_t>(ptr + 6);
    }
    else if constexpr (SZ == 6) {
        MemRegion::write<uint32_t>(ptr);
        MemRegion::write<uint16_t>(ptr + 4);
    }
    else if constexpr (SZ == 5) {
        MemRegion::write<uint32_t>(ptr);
        MemRegion::write<uint8_t>(ptr + 4);
    }
    else if constexpr (SZ == 4) {
        MemRegion::write<uint32_t>(ptr);
    }
    else if constexpr (SZ == 3) {
        MemRegion::write<uint16_t>(ptr);
        MemRegion::write<uint8_t>(ptr + 2);
    }
    else if constexpr (SZ == 2) {
        MemRegion::write<uint32_t>(ptr);
    }
    else if constexpr (SZ == 1) {
        MemRegion::write<uint32_t>(ptr);
    }
    else static_assert(SZ != 0);
}

template<const uint16_t SZ> void write_nt_t(uint8_t * const ptr)
{
    if constexpr (SZ >= 64) { // 512
        MemRegion::write_nt<__m512>(ptr);
        write_nt_t<SZ - 64>(ptr + 64);
    }
    else if constexpr (SZ >= 32) { // 256
        MemRegion::write_nt<__m256>(ptr);
        write_nt_t<SZ - 32>(ptr + 32);
    }
    else if constexpr (SZ >= 16) { // 128
        MemRegion::write_nt<__m128>(ptr);
        write_nt_t<SZ - 16>(ptr + 16);
    }
    else if constexpr (SZ == 8) { // 64
        MemRegion::write_nt<__m64>(ptr);
    }
    else static_assert(SZ == 0);
}

template<const uint16_t SZ> __attribute__((noinline))
Elapsed measure_read_t(uint8_t * ptr)
{
    StopWatch sw;
    read_t<SZ>(ptr);
    return sw.stop();
}

template<const uint16_t SZ> __attribute__((noinline))
Elapsed measure_write_t(uint8_t * const ptr)
{
    StopWatch sw;
    write_t<SZ>(ptr);
    return sw.stop();
}

template<const uint16_t SZ> __attribute__((noinline))
Elapsed measure_write_mfence_t(uint8_t * const ptr)
{
    StopWatch sw;
    write_t<SZ>(ptr);
    _mm_mfence();
    return sw.stop();
}

template<const uint16_t SZ> __attribute__((noinline))
Elapsed measure_write_nt_t(uint8_t * const ptr)
{
    StopWatch sw;
    write_nt_t<SZ>(ptr);
    return sw.stop();
}

Elapsed MemRegion::measure_nope(const std::size_t off, const uint16_t size) const
{
    StopWatch sw;
    //??StopWatch __sw; __sw.stop();
    return sw.stop();
}

Elapsed MemRegion::measure_read(const std::size_t off, const uint16_t size) const
{
    switch(size) {
    case 512: return measure_read_t<512>(m_ptr + off);
    case 256: return measure_read_t<256>(m_ptr + off);
    case 128: return measure_read_t<128>(m_ptr + off);
    case  64: return measure_read_t< 64>(m_ptr + off);
    case  32: return measure_read_t< 32>(m_ptr + off);
    case  16: return measure_read_t< 16>(m_ptr + off);
    case   8: return measure_read_t<  8>(m_ptr + off);
    case   7: return measure_read_t<  7>(m_ptr + off);
    case   6: return measure_read_t<  6>(m_ptr + off);
    case   5: return measure_read_t<  5>(m_ptr + off);
    case   4: return measure_read_t<  4>(m_ptr + off);
    case   3: return measure_read_t<  3>(m_ptr + off);
    case   2: return measure_read_t<  2>(m_ptr + off);
    case   1: return measure_read_t<  1>(m_ptr + off);
    default: THROW_ERROR("Can't measure read with size " << size);
    }
}

Elapsed MemRegion::measure_write(const std::size_t off, const uint16_t size) const
{
    switch(size) {
    case 512: return measure_write_t<512>(m_ptr + off);
    case 256: return measure_write_t<256>(m_ptr + off);
    case 128: return measure_write_t<128>(m_ptr + off);
    case  64: return measure_write_t< 64>(m_ptr + off);
    case  32: return measure_write_t< 32>(m_ptr + off);
    case  16: return measure_write_t< 16>(m_ptr + off);
    case   8: return measure_write_t<  8>(m_ptr + off);
    case   7: return measure_write_t<  7>(m_ptr + off);
    case   6: return measure_write_t<  6>(m_ptr + off);
    case   5: return measure_write_t<  5>(m_ptr + off);
    case   4: return measure_write_t<  4>(m_ptr + off);
    case   3: return measure_write_t<  3>(m_ptr + off);
    case   2: return measure_write_t<  2>(m_ptr + off);
    case   1: return measure_write_t<  1>(m_ptr + off);
    default: THROW_ERROR("Can't measure write with size " << size);
    }
}

template<const uint16_t SZ> __attribute__((noinline))
void read_clflush_t(uint8_t * ptr)
{
    if constexpr (SZ > 64) {
        read_clflush_t<64>(ptr);
        read_clflush_t<SZ - 64>(ptr + 64);
        return;
    }

    read_t<SZ>(ptr);
    _mm_clflush(ptr);
    //for(i=0;i<=size+64;i+=64){
    //    _mm_clflush(buf+i+offset);
    //}
}

template<const uint16_t SZ> __attribute__((noinline))
Elapsed measure_read_clflush_t(uint8_t * ptr)
{
    StopWatch sw;
    read_clflush_t<SZ>(ptr);
    return sw.stop();
}

Elapsed MemRegion::measure_read_clflush(std::size_t off, uint16_t size) const
{
    switch(size) {
    case 512: return measure_read_clflush_t<512>(m_ptr + off);
    case 256: return measure_read_clflush_t<256>(m_ptr + off);
    case 128: return measure_read_clflush_t<128>(m_ptr + off);
    case  64: return measure_read_clflush_t< 64>(m_ptr + off);
    case  32: return measure_read_clflush_t< 32>(m_ptr + off);
    case  16: return measure_read_clflush_t< 16>(m_ptr + off);
    case   8: return measure_read_clflush_t<  8>(m_ptr + off);
    case   7: return measure_read_clflush_t<  7>(m_ptr + off);
    case   6: return measure_read_clflush_t<  6>(m_ptr + off);
    case   5: return measure_read_clflush_t<  5>(m_ptr + off);
    case   4: return measure_read_clflush_t<  4>(m_ptr + off);
    case   3: return measure_read_clflush_t<  3>(m_ptr + off);
    case   2: return measure_read_clflush_t<  2>(m_ptr + off);
    case   1: return measure_read_clflush_t<  1>(m_ptr + off);
    default: THROW_ERROR("Can't measure read with size " << size);
    }
}

Elapsed MemRegion::measure_write_mfence(const std::size_t off, const uint16_t size) const
{
    switch(size) {
    case 512: return measure_write_mfence_t<512>(m_ptr + off);
    case 256: return measure_write_mfence_t<256>(m_ptr + off);
    case 128: return measure_write_mfence_t<128>(m_ptr + off);
    case  64: return measure_write_mfence_t< 64>(m_ptr + off);
    case  32: return measure_write_mfence_t< 32>(m_ptr + off);
    case  16: return measure_write_mfence_t< 16>(m_ptr + off);
    case   8: return measure_write_mfence_t<  8>(m_ptr + off);
    case   7: return measure_write_mfence_t<  7>(m_ptr + off);
    case   6: return measure_write_mfence_t<  6>(m_ptr + off);
    case   5: return measure_write_mfence_t<  5>(m_ptr + off);
    case   4: return measure_write_mfence_t<  4>(m_ptr + off);
    case   3: return measure_write_mfence_t<  3>(m_ptr + off);
    case   2: return measure_write_mfence_t<  2>(m_ptr + off);
    case   1: return measure_write_mfence_t<  1>(m_ptr + off);
    default: THROW_ERROR("Can't measure mfence write with size " << size);
    }
}


Elapsed MemRegion::measure_write_nt(const std::size_t off, const uint16_t size) const
{
    switch(size) {
    case  512: return measure_write_nt_t<512>(m_ptr + off);
    case  256: return measure_write_nt_t<256>(m_ptr + off);
    case  128: return measure_write_nt_t<128>(m_ptr + off);
    case   64: return measure_write_nt_t< 64>(m_ptr + off);
    case   32: return measure_write_nt_t< 32>(m_ptr + off);
    case   16: return measure_write_nt_t< 16>(m_ptr + off);
    case    8: return measure_write_nt_t<  8>(m_ptr + off);
    default: THROW_ERROR("Can't measure non-temporal write with size " << size);
    }
}


void MemRegion::load_all_pages() { MemRegion::load_pages(m_ptr, m_size); }

void MemRegion::load_pages(void * ptr, const std::size_t sz)
{
    STATUSLOG("Start page loading (" << (static_cast<double>(sz) / (2 << (30-1))) << "Gb)...");

    auto p = reinterpret_cast<uint8_t *>(ptr);
    StopWatch sw;
    #pragma omp parallel for
    for (std::size_t i = 0; i < sz; i += PAGE_SIZE) {
        //MemRegion::write<uint64_t>(p + i);
        MemRegion::read<uint64_t>(p + i);
    }
    sw.stop();
    DEBUGLOG(sz / PAGE_SIZE << " pages loading took: " << sw.msecs() << "ms");
}

bool MemRegion::bind(void * ptr, std::size_t sz, const nodemask_t nodes)
{
    static const auto maxnode = 1 << numa_max_node();
    unsigned long int nodes_long =  nodes.to_ulong();
    ptr = reinterpret_cast<void *>(reinterpret_cast<uint64_t>(ptr) / MemRegion::PAGE_SIZE * MemRegion::PAGE_SIZE);
    //DEBUGLOG(std::hex << ptr << " " << sz << " " << std::dec  << nodes);
    return 0 == ::mbind(ptr, sz, nodes.count() > 1 ? MPOL_INTERLEAVE : MPOL_BIND, &nodes_long, maxnode, MPOL_MF_MOVE);
}

MemRegion::~MemRegion() { ::munmap(m_ptr, m_size); }

void MemRegion::clflush_ptr(void * ptr, std::size_t sz)
{
    auto p = static_cast<uint8_t *>(ptr);
    _mm_clflush(p);
    while(sz > 64) {
        sz -= 64;
        p += 64;
        _mm_clflush(p);
    }
}


Mem::Mem()
  : m_bind(false)
  , m_load(true)
  , m_read(true)
  , m_write(true)
  , m_node(0)
  , m_ratio(1)
  , m_fd(-1)
{}

Mem::~Mem()
{
    if (m_fd != -1) {
        ::close(m_fd);
    }
}

Mem::Mem(const Config & cfg) : Mem() { setup(cfg); }
Mem & Mem::setup(const Config & cfg)
{
    m_fd = -1;
    m_node = 0;
    m_ratio = cfg.get_remote_ratio();
    if (const auto & f = cfg.get_remote_file(); !f.empty()) {
        if (-1 == (m_fd = open(f.c_str(), O_RDWR | O_CREAT, 0666))) {
            THROW_ERROR("ERROR: can't open memory file - " << strerror(errno));
        }
        if (::lseek(m_fd, cfg.get_working_set_size() - 1, SEEK_SET) == -1
        ||  ::write(m_fd, "", 1) != 1) {
            THROW_ERROR("ERROR: can't resize memory file - " << strerror(errno));
        }
        //if (params.file_path) {
        //    fd = open(params.file_path, O_RDWR, 0666);
        //    if (fd == -1) {
        //        prn("ERROR: can't open device file.\n");
        //        return 1;
        //    }
        //}
    }
    else if (const auto & r = cfg.get_remote_node(); r) {
        m_bind = true;
        m_node = *r;
    }
    return * this;
}

MemRegion Mem::allocate(const std::size_t sz) const
{
    MemRegion region(mmap(sz), sz);

    if (m_bind) {
        int local_node = numa_preferred();
        int remote_node = m_node;

        if (local_node == remote_node) {
            if (!region.bind(nodemask_t().set(m_node), sz)) {
                THROW_ERROR("mbind failed: " << std::strerror(errno));
            }
            STATUSLOG("Remote and local nodes are the same. Successfully bind to " << m_node << " (" << sz/ 1024/1024 << "Mb)");
        }
        else {
            std::size_t remote_sz = sz * m_ratio;
            std::size_t local_sz = sz - remote_sz;
            if (remote_sz > local_sz) {
                std::swap(remote_sz, local_sz);
                std::swap(remote_node, local_node);
            }

            nodemask_t msk; msk.set(local_node).set(remote_node);
            const auto bind_size = 2 * remote_sz;
            if (bind_size > 0) {
                if (!region.bind(msk, bind_size)) {
                    THROW_ERROR("mbind failed: " << std::strerror(errno));
                }
                STATUSLOG("Successfully interleaved between nodes " << remote_node << " and " << local_node << " (" << bind_size / 1024/1024 << "Mb)");
            }
            if (sz - bind_size > 0) {
                msk.reset(remote_node);
                if (!region.bind(msk, sz - bind_size - 1, /*offset:*/bind_size)) {
                    THROW_ERROR("mbind(msk:" << msk << ", sz:" << sz - bind_size  << ", offs:" << bind_size << ") failed: " << std::strerror(errno));
                }
                STATUSLOG("Successfully bind to node " << local_node << " (" << (sz - bind_size) / 1024/1024 << "Mb)");
            }
        }
    }

    if (m_load) {
        region.load_all_pages();
    }
    else {
        DEBUGLOG("Page loading skipped");
    }
    return region;
}

void * Mem::mmap(const std::size_t sz) const
{
    int permissions = 0;
    if (m_read) {
        permissions |= PROT_READ;
    }
    if (m_write) {
        permissions |= PROT_WRITE;
    }

    //int opt = MAP_ANONYMOUS | MAP_PRIVATE;
    //int opt = MAP_SHARED;
    int opt = MAP_PRIVATE | MAP_POPULATE;
    if (m_fd == -1) {
        opt |= MAP_ANONYMOUS;
    }

    void * ptr = ::mmap((void*)0x100000000, sz, permissions, opt, m_fd, 0);
    if (ptr == MAP_FAILED) {
        THROW_ERROR("mmap failed: " << std::strerror(errno));
    }

    STATUSLOG("Region: " << std::hex << ptr<< " - " << (void*)(static_cast<char *>(ptr) + sz) << std::dec);
    return ptr;
}



}
