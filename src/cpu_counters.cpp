#include "cpu_counters.h"
#include "log.h"

#ifdef USE_PCM
#include "pcm/cpucounters.h"
#endif

namespace memx {

bool CPUCounters::is_available() {
#ifdef USE_PCM
    return pcm::PCM::getInstance()->PMMTrafficMetricsAvailable();
#else
    return false;
#endif
}

#ifdef USE_PCM

class CPUCounters::Impl {
public:
    Impl()
    {
        m_pcm->disableJKTWorkaround();
        const auto status = m_pcm->programServerUncoreMemoryMetrics(
                pcm::ServerUncoreMemoryMetrics::Pmem);

        switch (status)
        {
        case pcm::PCM::Success:
            break;
        case pcm::PCM::MSRAccessDenied:
            //cerr << "Access to Processor Counter Monitor has denied (no MSR or PCI CFG space access).\n";
            THROW_ERROR("MSR: AccessDenied");
        case pcm::PCM::PMUBusy:
            THROW_ERROR("PMU is busy");
        default:
            THROW_ERROR("Access to Processor Counter Monitor has denied (Unknown error).");
        }

        m_pcm->setBlocked(true);
        const auto sockets = m_pcm->getNumSockets();
        m_state_after.resize(sockets);
        m_state_before.resize(sockets);
        for (size_t i = 0; i < sockets; ++i) {
            m_state_before[i] = m_pcm->getServerUncoreCounterState(i);
        }
        m_time_before = m_pcm->getTickCount();
    }

    void check_state() {
        m_time_after = m_pcm->getTickCount();
        const auto sockets = m_pcm->getNumSockets();
        for (size_t i = 0; i < sockets; ++i) {
            m_state_after[i] = m_pcm->getServerUncoreCounterState(i);
        }
        auto elapsed_time = m_time_after - m_time_before;
        for(uint32_t skt = 0; skt < sockets; ++skt) {
            const auto numChannels1 = m_pcm->getMCChannels(skt, 0); // number of channels in the first controller

            auto toBW = [&elapsed_time](const uint64_t nEvents) {
                    return (float)(nEvents * 64 / 1000000.0 / (elapsed_time / 1000.0));
                };
    /        for (uint32_t channel = 0; channel < max_imc_channels; ++channel)
    //        {
    //            uint64 reads = 0, writes = 0, pmmReads = 0, pmmWrites = 0, pmmMemoryModeCleanMisses = 0, pmmMemoryModeDirtyMisses = 0;
    //            uint64 pmmMemoryModeHits = 0;
    //            reads = getMCCounter(channel, ServerPCICFGUncore::EventPosition::READ, uncState1[skt], uncState2[skt]);
    //            writes = getMCCounter(channel, ServerPCICFGUncore::EventPosition::WRITE, uncState1[skt], uncState2[skt]);
    //            if (metrics == Pmem)
    //            {
    //                pmmReads = getMCCounter(channel, ServerPCICFGUncore::EventPosition::PMM_READ, uncState1[skt], uncState2[skt]);
    //                pmmWrites = getMCCounter(channel, ServerPCICFGUncore::EventPosition::PMM_WRITE, uncState1[skt], uncState2[skt]);
    //            }
    //            else if (metrics == PmemMixedMode || metrics == PmemMemoryMode)
    //            {
    //                pmmMemoryModeCleanMisses = getMCCounter(channel, ServerPCICFGUncore::EventPosition::PMM_MM_MISS_CLEAN, uncState1[skt], uncState2[skt]);
    //                pmmMemoryModeDirtyMisses = getMCCounter(channel, ServerPCICFGUncore::EventPosition::PMM_MM_MISS_DIRTY, uncState1[skt], uncState2[skt]);
    //            }
    //            if (metrics == PmemMemoryMode)
    //            {
    //                pmmMemoryModeHits = getMCCounter(channel, ServerPCICFGUncore::EventPosition::NM_HIT, uncState1[skt], uncState2[skt]);
    //            }
    //            if (skipInactiveChannels && (reads + writes == 0))
    //            {
    //                if ((metrics != Pmem) || (pmmReads + pmmWrites == 0))
    //                {
    //                    if ((metrics != PmemMixedMode) || (pmmMemoryModeCleanMisses + pmmMemoryModeDirtyMisses == 0))
    //                    {

    //                        md.iMC_Rd_socket_chan[skt][channel] = -1.0;
    //                        md.iMC_Wr_socket_chan[skt][channel] = -1.0;
    //                        continue;
    //                    }
    //                }
    //            }

    //            if (metrics != PmemMemoryMode)
    //            {
    //                md.iMC_Rd_socket_chan[skt][channel] = toBW(reads);
    //                md.iMC_Wr_socket_chan[skt][channel] = toBW(writes);

    //                md.iMC_Rd_socket[skt] += md.iMC_Rd_socket_chan[skt][channel];
    //                md.iMC_Wr_socket[skt] += md.iMC_Wr_socket_chan[skt][channel];
    //            }

    //            if (metrics == Pmem)
    //            {
    //                md.iMC_PMM_Rd_socket_chan[skt][channel] = toBW(pmmReads);
    //                md.iMC_PMM_Wr_socket_chan[skt][channel] = toBW(pmmWrites);

    //                md.iMC_PMM_Rd_socket[skt] += md.iMC_PMM_Rd_socket_chan[skt][channel];
    //                md.iMC_PMM_Wr_socket[skt] += md.iMC_PMM_Wr_socket_chan[skt][channel];

    //                md.M2M_NM_read_hit_rate[skt][(channel < numChannels1) ? 0 : 1] += (float)reads;
    //            }
    //            else if (metrics == PmemMixedMode)
    //            {
    //                md.iMC_PMM_MemoryMode_Miss_socket_chan[skt][channel] = toBW(pmmMemoryModeCleanMisses + 2 * pmmMemoryModeDirtyMisses);
    //                md.iMC_PMM_MemoryMode_Miss_socket[skt] += md.iMC_PMM_MemoryMode_Miss_socket_chan[skt][channel];
    //            }
    //            else if (metrics == PmemMemoryMode)
    //            {
    //                md.iMC_PMM_MemoryMode_Miss_socket[skt] += (pmmMemoryModeCleanMisses + pmmMemoryModeDirtyMisses) / (elapsed_time / 1000.0);
    //                md.iMC_PMM_MemoryMode_Hit_socket[skt] += (pmmMemoryModeHits) / (elapsed_time / 1000.0);
    //            }
    //            else
    //            {
    //                md.partial_write[skt] += (uint64)(getMCCounter(channel, ServerPCICFGUncore::EventPosition::PARTIAL, uncState1[skt], uncState2[skt]) / (elapsed_time / 1000.0));
    //            }
    //        }
    //    }
        //    if (metrics == PmemMemoryMode)
        //    {
        //        md.iMC_Rd_socket[skt] += toBW(getFreeRunningCounter(ServerUncoreCounterState::ImcReads, uncState1[skt], uncState2[skt]));
        //        md.iMC_Wr_socket[skt] += toBW(getFreeRunningCounter(ServerUncoreCounterState::ImcWrites, uncState1[skt], uncState2[skt]));
        //    }
        //    if (metrics == PmemMixedMode || metrics == PmemMemoryMode)
        //    {
        //        const int64 pmmReads = getFreeRunningCounter(ServerUncoreCounterState::PMMReads, uncState1[skt], uncState2[skt]);
        //        if (pmmReads >= 0)
        //        {
        //            md.iMC_PMM_Rd_socket[skt] += toBW(pmmReads);
        //        }
        //        else for(uint32_t c = 0; c < max_imc_controllers; ++c)
        //        {
        //            md.iMC_PMM_Rd_socket[skt] += toBW(getM2MCounter(c, ServerPCICFGUncore::EventPosition::PMM_READ, uncState1[skt],uncState2[skt]));
        //        }

        //        const int64 pmmWrites = getFreeRunningCounter(ServerUncoreCounterState::PMMWrites, uncState1[skt], uncState2[skt]);
        //        if (pmmWrites >= 0)
        //        {
        //            md.iMC_PMM_Wr_socket[skt] += toBW(pmmWrites);
        //        }
        //        else for(uint32_t c = 0; c < max_imc_controllers; ++c)
        //        {
        //            md.iMC_PMM_Wr_socket[skt] += toBW(getM2MCounter(c, ServerPCICFGUncore::EventPosition::PMM_WRITE, uncState1[skt],uncState2[skt]));;
        //        }
        //    }
        //    if (metrics == Pmem)
        //    {
        //        for(uint32_t c = 0; c < max_imc_controllers; ++c)
        //        {
        //            if(md.M2M_NM_read_hit_rate[skt][c] != 0.0)
        //            {
        //                md.M2M_NM_read_hit_rate[skt][c] = ((float)getM2MCounter(c, ServerPCICFGUncore::EventPosition::NM_HIT, uncState1[skt],uncState2[skt]))/ md.M2M_NM_read_hit_rate[skt][c];
        //            }
        //        }
        //    }
        //    const auto all = md.iMC_PMM_MemoryMode_Miss_socket[skt] + md.iMC_PMM_MemoryMode_Hit_socket[skt];
        //    if (metrics == PmemMemoryMode && all != 0.0)
        //    {
        //        md.iMC_NM_hit_rate[skt] = md.iMC_PMM_MemoryMode_Hit_socket[skt] / all;
        //    }
        }

        std::swap(m_state_before, m_state_after);
        std::swap(m_time_before, m_time_after);
    }

private:
    //pcm::SystemCounterState m_counters = pcm::PCM::getInsance()->getSystemCounterState();
 //   pcm::ServerUncoreMemoryMetrics m_metrics;
    pcm::PCM * m_pcm = pcm::PCM::getInstance();
    std::vector<pcm::ServerUncoreCounterState> m_state_before;
    std::vector<pcm::ServerUncoreCounterState> m_state_after;
    uint64_t m_time_before = 0;
    uint64_t m_time_after = 0;
};

#else
  class CPUCounters::Impl { };
#endif

CPUCounters::CPUCounters()
  : m_impl(std::make_unique<Impl>())
{ }

CPUCounters::~CPUCounters() = default;


} // namespace memx
