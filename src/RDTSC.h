
#include <cstdint>

namespace memx {

inline uint64_t RDTSC()
{
    uint64_t hi; uint32_t lo;
    __asm__ __volatile__("lfence;rdtsc;lfence;" : "=a" (lo), "=d" (hi));
    //__asm__ __volatile__("rdtscp" : "=a" (lo), "=d" (hi) : : "rcx");
    //__asm__ __volatile__("mfence;rdtsc" : "=a" (lo), "=d" (hi));
    return hi << 32 | lo;
}


uint64_t TSC_init_rate();
uint64_t TSC_to_msec(uint64_t tsc);
uint64_t TSC_to_usec(uint64_t tsc);
uint64_t TSC_to_nsec(uint64_t tsc);
double TSC_to_sec(uint64_t tsc);

uint64_t usec_to_TSC(uint64_t usec);


}
