#include "Config.h"
#include "access_pattern.h"
#include "forward.h"
#include "log.h"
#include "mem.h"
#include "watch.h"
#include "histogram.h"
#include "exec.h"

#include <json/json.h>
#include <numa.h>
#include <omp.h>
#include <sys/types.h>
#include <sched.h>

#include <functional>
#include <cstring>
#include <fstream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>
#include <unistd.h>
#include <vector>

namespace memx {

class PerformanceTest {

    using clock = std::chrono::system_clock;

public:
    PerformanceTest(const Config & cfg)
        : m_cfg(cfg)
        , m_mem(cfg)
    {
        logging_level_set(cfg);
    }

    bool bind_memory_manager()
    {
        if (m_cfg.get_bind().empty()) {
            return false;
        }
        else if (m_cfg.get_bind() == "kmod") {
            const auto pid = getpid();
            std::ofstream kmod_fs("/proc/kmod");
            if (kmod_fs) {
                STATUSLOG("Joining to kernel module...");
                kmod_fs << "bind " << pid << std::endl;
                return true;
            }
            m_unbind_memory_manager = [pid] {
                    std::ofstream kmod_fs("/proc/kmod");
                    if (kmod_fs) {
                        STATUSLOG("Leaving kernel module...");
                        kmod_fs << "unbind " << pid << std::endl;
                    }
                };
        }
        else if (m_cfg.get_bind() == "ambix") {
            const auto pid = getpid();
            exec(std::string("cd ../ambix; ./src/bind.o ") + std::to_string(pid));
            m_unbind_memory_manager = [pid] {
                    exec(std::string("cd ../ambix; ./src/unbind.o ") + std::to_string(pid));
                };
        }
        else {
            THROW_ERROR("Unknown bind method: " << m_cfg.get_bind());
        }

        return false;
    }

    bool run()
    {
        MemRegion p = m_mem.allocate(m_cfg.get_working_set_size());
        if (const auto v = m_cfg.get_num_threads(); v) {
            omp_set_num_threads(*v);
        }

        std::ofstream access_log;
        if (!m_cfg.get_access_log().empty()) {
            access_log.open(m_cfg.get_access_log());
        }

        if (access_log) {
            access_log << "Region: " << std::hex << (const void *) p.get_pointer() <<
                " - " << (const void*)(p.get_pointer() + p.get_size()) << std::endl;
        }

        bind_memory_manager(); // if any
        const auto wmode = parse_write_mode(m_cfg.get_write_mode());
        const auto rmode = parse_read_mode(m_cfg.get_read_mode());
        const auto sz = m_cfg.get_touch_bytes();
        auto secs = m_cfg.get_limit_seconds().count();
        const auto time_limit =  usec_to_TSC((secs ? secs : 60) * 1'000'000);
        STATUSLOG("Starting memory test...");
        #pragma omp parallel
        {
            const auto thnum = omp_get_num_threads();
            const auto op_limit = (m_cfg.get_limit_operations() * 10 + 5) / thnum / 10;

            Histogram hist, nope;

            const auto thn = omp_get_thread_num();
            generator_t random_generator(m_cfg.get_seed() + thn);

            auto access_pattern =
                IAccessPattern::create(m_cfg, random_generator);

            uint64_t nope_time {0};
            uint64_t access_time {0};
            uint64_t operation_count {0};
            uint64_t now, start = RDTSC();
            for (now = start; now - start < time_limit; now = RDTSC()) {
                const uint64_t offset = access_pattern->next_offset();
                const auto op = access_pattern->next_operation();
                uint64_t t;
                switch (op) {
                case Operation::Read:
                    switch (rmode) {
                    case ReadMode::empty:
                        t = p.measure_read(offset, sz).nsecs();
                        break;
                    case ReadMode::clflush:
                        t = p.measure_read_clflush(offset, sz).nsecs();
                        break;
                    }
                    break;
                case Operation::Write:
                    switch (wmode) {
                    case WriteMode::empty:
                        t = p.measure_write(offset, sz).nsecs();
                        break;
                    case WriteMode::nt:
                        t = p.measure_write_nt(offset, sz).nsecs();
                        break;
                    case WriteMode::mfence:
                        t = p.measure_write_mfence(offset, sz).nsecs();
                        break;
                    case WriteMode::clwb:
                        THROW_ERROR("Measure function was not implemented");
                    }
                    break;
                default:
                    THROW_ERROR("Unknown operation");
                }
                access_time += t;
                hist.add_sample(t);

                //if (thn == 0) {
                ////    DEBUGLOG(offset * 100 / m_cfg.get_working_set_size() << "%: " << t);
                //    DEBUGLOG(offset << ": " << t);
                //}
                if (m_cfg.get_clfence_after_op()) {
                    p.clflush(offset, sz);
                }

                t = p.measure_nope(offset, sz).nsecs();
                nope_time += t;
                nope.add_sample(t);

                ++operation_count;
                if (operation_count > op_limit && op_limit != 0) {
                    DEBUGLOG(thn << ": reached operations limit (" << op_limit << ").");
                    break;
                }

                //DDUMP("off: " << std::hex << offset << std::dec << " op: " <<
                //       (op == Operation::Read ? "r" : "w"));
                if (access_log) {
                    #pragma omp critical
                    access_log << "off: " << std::hex << offset << std::dec << " op: " <<
                           (op == Operation::Read ? "r\n" : "w\n");
                }
            }
            #pragma omp critical
            {
                m_nope.merge(nope);
                m_hist.merge(hist);

                //DEBUGLOG(thn << ": sz=" << sz << "; op_count="<< operation_count << "; access_time=" << access_time << "; nop_time=" << nope_time << ".");
                m_throughput          += sz * operation_count * 1000 / access_time; // (Mbps)
                m_throughput_adjusted += sz * operation_count * 1000 / (access_time - nope_time); // (Mbps)

                m_total_access_nsecs += access_time;
                m_total_nope_nsecs += nope_time;
                m_max_access_nsecs = std::max(m_max_access_nsecs, access_time);
                m_operations_count += operation_count;
                ++m_workers_count;
                m_run_time = std::max(m_run_time, now - start);

                if (thn == 0) {
                    std::ostringstream oss;
                    access_pattern->flush_description(oss);
                    m_pattern_description = oss.str();
                }
            }
        }
        m_unbind_memory_manager(); // if any
        STATUSLOG("Memory test complete");

        if (!m_cfg.get_pmap_file().empty()) {
            std::ifstream ifs("/proc/self/maps");
            std::ofstream ofs(m_cfg.get_pmap_file());
            if (!ifs or !ofs) {
                STATUSLOG("Can't populate pmap file");
            }
            else {
                ofs << ifs.rdbuf();
            }
        }
        return true;
    }

    void flush_statistics(std::ostream & oss)
    {
//        if (cpu_set_t c; !sched_getaffinity(0/*self*/, sizeof(c), &c)) {
//            oss << "affinity: "
//                numa_max_possible_node
//                numa_get_mems_allowed()
        //}
        oss << m_operations_count << " access by " << m_workers_count << " threads took " << m_total_access_nsecs / 1'000'000  << "ms."
            " The longest thread run: " << m_max_access_nsecs / 1'000'000 << "ms; running  time " << TSC_to_msec(m_run_time) / 1'000 << " seconds\n"
            "     pattern: " << m_pattern_description << "\n"
            //"     pattern: " << m_cfg.get_access_pattern() << " (stride: " << m_cfg.get_stride() << " bytes)\n"
            "     ws size: " << m_cfg.get_working_set_size() / 1024 / 1024 << "Mib\n"
            "     wt mode: " << (m_cfg.get_write_mode().empty() ? "empty" : m_cfg.get_write_mode()) << "\n"
            //"   rd amount: " << static_cast<unsigned>(m_cfg.get_read_ratio()) << "%\n"
            "     op size: " << m_cfg.get_touch_bytes() << "bytes [aligned by " << m_cfg.get_alignment() << "]\n"
            << std::fixed << std::setprecision(3) <<
            "    avg tput: " << static_cast<double>(m_operations_count) * m_cfg.get_touch_bytes() * 1000 / m_total_access_nsecs << "Mb/s x " << m_workers_count << "thd\n"
            "        tput: " << m_throughput << "Mb/s\n"
            << std::setprecision(0) <<
            " latency(ns): 50p=" << m_hist.get_percentile(50) << ";"
            << std::setprecision(2) <<
            " μ="  << m_hist.get_mean() << "; σ="  << m_hist.get_deviation() << ";\n"
            << std::setprecision(0) <<
            "              min=" <<  m_hist.get_percentile(0) << "; 99p=" <<  m_hist.get_percentile(99) << "; max=" << m_hist.get_percentile(100) << ";\n"
            << std::setprecision(2) <<
            "    overhead: 50p=" << m_nope.get_median() << "; μ=" << m_nope.get_mean() << "; σ=" << m_nope.get_deviation() << ";\n"
            "\n"
            << std::setprecision(0) <<
            "    adj  lat: "  << m_hist.get_median() - m_nope.get_median() << "ns\n"
            "    adj tput: " << m_throughput_adjusted << "Mb/s\n";
    }


private:
    const Config & m_cfg;
    memx::Mem m_mem;

    Histogram m_hist;
    Histogram m_nope;

    double m_throughput {0};
    uint64_t m_throughput_adjusted {0};

    uint64_t m_total_access_nsecs {0};
    uint64_t m_total_nope_nsecs {0};
    uint64_t m_max_access_nsecs {0};
    uint64_t m_operations_count {0};
    uint64_t m_workers_count {0};
    uint64_t m_run_time {0};

    std::string m_pattern_description;
    std::function<void()> m_unbind_memory_manager;
};

class MigrateTest
{
    memx::Mem m_mem;
    const Config & m_cfg;
public:
    MigrateTest(const Config & cfg)
        : m_mem(cfg)
        , m_cfg(cfg)
    {}

public:
    bool run() {
        MemRegion p = m_mem.allocate(m_cfg.get_working_set_size());
        STATUSLOG("Start Migration");
        if (0 != numa_migrate_pages(0,
                    numa_parse_nodestring("0"),
                    numa_parse_nodestring("1"))) {
            THROW_ERROR("numa_migrate_pgaes failed: " << std::strerror(errno));
        }
        STATUSLOG("Sleep for 5 seconds");
        std::this_thread::sleep_for(std::chrono::seconds(5));
        return true;
    }


    void flush_statistics(std::ostream & oss)
    {
        oss << "DONE\n";
    }
};

}

int main(int argc, char ** argv)
{
    using namespace memx;
    TSC_init_rate();

    if (argc == 3 && std::string("--test-suit") == argv[1]) {
        STATUSLOG("Reading test suit from '" << argv[2] << "'");
        const auto configs = Config::parse(argv[2]);
        return 0;
    }

    if (argc == 2 && std::string("--custom") == argv[1]) {
        memx::Config cfg;
        memx::Mem memory(cfg.set_remote_node(3));
        constexpr const size_t N = 4 * 1024;
        memx::StopWatch w;
        auto r = memory.allocate(N);
        std::cout << "allocation: " << w.stop().usecs() << "us\n";

        w.start();
        r.clflush<N>(0);
        std::cout << "clflushing: " << w.stop().usecs() << "us\n";
        const size_t o = 0;
        r.write_nt<__m64>(o);
        for (size_t i = 0; i < 10; ++i) {
            const size_t off = 0 * 256;
            r.clflush(off);
            //std::cout << "\n" << i << ": " << r.measure_write_nt(off, 8).nsecs() << "ns";
            w.start();
            r.write_nt<__m64>(off);
            r.mfence();
            const auto ns = w.stop().nsecs();

            std::cout << i << ": " << ns << "ns\n";
        }
        return 0;
    }

    Config config;
    config
        .set_working_set_size(1024ll*1024* 2 * 1024)
        .set_log_level("status")
        .set_access_pattern("uniform")
        .set_clflush_after_op()
        .set_read_ratio(50)
        .set_touch_bytes(8)
        .set_alignment(8)
//        .set_remote_node(2)
        .set_limit_seconds(std::chrono::seconds(60))
        ;
    //    //.set_access_pattern("linear")
    //    //.set_stride(256)
    //    //.set_stride(MemRegion::PAGE_SIZE)
    //    .set_seed(1024)
    //     .set_num_threads(1)
    if (!Config::parse_args(argc, argv, config)) {
        return -1;
    }

    //pid_t pid = getpid();
    //std::ofstream kmod_fs("/proc/kmod");
    //if (kmod_fs) {
    //    STATUSLOG("Joining to kernel module...");
    //    kmod_fs << pid << std::endl;
    //    kmod_fs.close();
    //}


    STATUSLOG("Initializing...");
    PerformanceTest test(config);


    //MigrateTest test(config);
    if (test.run()) {
        STATUSLOG("Flushing statistics...");
        test.flush_statistics(std::cout);
    }
    DEBUGLOG("Terminating...");
}
