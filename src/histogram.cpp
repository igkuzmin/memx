#include "histogram.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>

namespace memx {

//bool equals(const double lhs, const double rhs)
//{
//    return std::abs(lhs - rhs) < .00001;
//}

Histogram & Histogram::add_sample(const uint64_t v) { return add_sample(v, 1); }
Histogram & Histogram::add_sample(const uint64_t v, const size_t n)
{
    auto it = std::lower_bound(m_values.begin(), m_values.end(), Value(v));
    if (it == m_values.end() || v != it->value) {
        m_values.insert(it, Value(v, n));
    }
    else {
        it->count += n;
    }
    m_number += n;
    return *this;
}

void Histogram::merge(const Histogram & other)
{
    assert(this != &other);
    for (const auto & v: other.m_values) {
        add_sample(v.value, v.count);
    }
}

uint64_t Histogram::get_min() const
{ return m_values.front().value; }

uint64_t Histogram::get_max() const
{ return m_values.back().value; }

double Histogram::get_variance() const
{
    // -2- const double m = get_mean();
    // -2- double s = 0;
    // -2- for (const auto v: m_values) {
    // -2-     s += v.value * v.value * v.count / (m_number - 1);
    // -2- }
    // -2- return s - (m * m / (m_number - 1) * m_number);

    double a1 = 0;
    double a2 = 0;
    for (const auto v: m_values) {
        const double val = v.value;
        a1 +=       val * v.count / m_number;
        a2 += val * val * v.count / m_number;
    }
    return (a2 - a1 * a1);
}

double Histogram::get_mean() const
{
    double mean = 0;
    for (const auto s: m_values) {
        mean += static_cast<double>(s.value) * s.count / m_number;
    }
    return mean;
}

std::vector<DoubleValue> Histogram::get_density_average(size_t m) const
{
    const auto n = m_values.size();
    if (m > n) { m = n; }

    const double c = static_cast<double>(n) / m;

    std::vector<DoubleValue> density;
    density.resize(m);
    for (size_t i = 0, j = 0; i < n; ++i) {
        const size_t idx = std::floor(i / c);
        auto & v = density[idx];
        v.count += m_values[i].count;
        v.value += m_values[i].value * m_values[i].count;
        if (idx != std::floor((i + 1) / c) || i + 1 == n) { //last in group
            v.value /= v.count;
        }
    }
    return density;
}

std::vector<DoubleValue> Histogram::get_density_median(size_t m) const
{
    const auto n = m_values.size();
    if (m > n) { m = n; }

    const double c = static_cast<double>(n) / m;

    std::vector<DoubleValue> density;
    density.resize(m);
    for (size_t i = 0, j = 0; i < n; ++i) {
        const size_t idx = std::floor(i / c);
        auto & v = density[idx];
        v.count += m_values[i].count;
        if (idx != std::floor((i + 1) / c) || i + 1 == n) { //last in group
            for (int64_t midx = (v.count - 1) / 2 ; j < i; ++j) {
                midx -= m_values[j].count;
                if (midx >= 0) continue;
                break;
            }
            v.value = m_values[j].value;
            j = i;
        }
    }
    return density;
}

uint64_t Histogram::get_percentile(const double p) const
{
    const int64_t idx = (p / 100) * (m_number - 1) ;
    for (size_t i = 0, j = 0; i < m_values.size(); j += m_values[i++].count) {
        if (j + m_values[i].count > idx) {
            return m_values[i].value;
        }
    }
    return m_values.back().value;
}

} // memx 
