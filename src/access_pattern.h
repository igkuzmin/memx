#pragma once

#include "forward.h"

#include <random>
#include <memory>
#include <cstddef>

namespace memx {

using generator_t = std::default_random_engine;

enum class Operation { Read, Write };

class IAccessPattern
{
public:
    using uptr_t = std::unique_ptr<IAccessPattern>;

    virtual ~IAccessPattern() {}

public:
    virtual uint64_t next_offset() = 0;
    virtual Operation next_operation() = 0;
    virtual void flush_description(std::ostream &) = 0;

    static uptr_t create(const Config &, generator_t &);
};

}
