#!/bin/python
import re
import fileinput
import sys
from record import AddressSpace, Record

rs = AddressSpace(int(0x100000/4)) #split on MB / 4 buckets
ws = AddressSpace()

with open(sys.argv[1], 'r') as f:
    l = 0
    sz = 0
    for line in f.readlines():
        m = re.match('Region: 0x([0-9a-f]+) - 0x([0-9a-f]+)', line)
        if m:
            l = int(m.group(1), 16)
            sz = int(m.group(2), 16) - l
        else:
            m = re.match('off: ([0-9a-f]+) op: (.)', line)
            if m:
                off = int(m.group(1), 16)
                if off > sz:
                    raise Exception("Out of boundaries: {:x} > {:x}".format(off, sz))
                rec = Record(off + l)
                if m.group(2) == 'r':
                    rs.add(rec)
                else:
                    ws.add(rec)

n = rs.sample_num()
#dots = False
last = None
for v in rs.values_by_va():
    if last is not None:
        if v.va - last > rs.mask():
            print('...')
    print("{:x}".format(v.va), '@' * int(v.nu / n * 600))
    last = v.va
