from trace import Trace, TraceReader

import logging
import math

from logging import DEBUG, INFO


def pfn(va):
    return va & (-1 << 12)

class Simulator:
    LOG = logging.getLogger("Sim")

    def __init__(self, simulations = []):
        self.__simulations = simulations

    def add_simulation(self, simulation):
        self.__simulations.append(simulation)

    def simulations(self):
        return self.__simulations

    def run(self, tracefile):
        from datetime import datetime
        trace = TraceReader(tracefile)
        n = trace.header.records
        i = 0
        self.LOG.info("Read trace from '%s' (%d records)", tracefile, n)
        last = datetime.now()
        last_tick = None
        for t in trace:
            if last_tick is None:
                last_tick = t.time
            i += 1
            now = datetime.now()
            if (now - last).seconds >= 5:
                self.LOG.info("Processed %2d%% of records (+%d ticks)",
                        int(i / n * 100), t.time - last_tick)
                last = now
                last_tick = t.time

            for sim in self.__simulations:
                sim.handle_tracepoint(t)
        return 0


class ClassifierStats:
    def __init__(self, TP = 0, FP = 0, TN = 0, FN = 0, temp = 0, base_temp = 0, CN = 0):
        self.TP = TP
        self.FP = FP
        self.TN = TN
        self.FN = FN
        self.CN = CN
        self.temp = temp
        self.base_temp = base_temp

    def Precision(self):
        if self.TP + self.FP == 0: return None
        return self.TP / (self.TP + self.FP)

    def Recall(self):
        if self.TP + self.FN == 0: return None
        return self.TP / (self.TP + self.FN)

    def Accuracy(self):
        if self.TP + self.FN + self.TN + self.FP == 0: return None
        return (self.TP + self.TN) / (self.TP + self.FN + self.TN + self.FP)

    def F1Score(self):
        if self.TP + self.FP + self.FN == 0: return None
        return 2 * self.TP / (2 * self.TP + self.FP + self.FN)

    def Tacc(self):
        if self.base_temp == 0: return None
        return self.temp / self.base_temp

    @property
    def Total(self):
        return self.TP + self.FP + self.TN + self.FN

class Simulation:
    PAGE_MASK = -1 << 12

    def __init__(self, name = "Simulation"):
        self.LOG = logging.getLogger(name)
        self.name = name
        self.stat = {}
        self._internal_action = None

    def set_internal_action_handler(self, func):
        self._internal_action = func

    def trigger_internal_action(self):
        if self._internal_action is not None:
            self._internal_action(self)

    def handle_tracepoint(self): raise NotImplementedError()

class ActionSimulation(Simulation):

    def __init__(self, action_interval, name = 'ActionSimulation'):
        super().__init__(name)
        self.__action_interval = action_interval
        self.reset()

    def reset(self):
        self.__last_action = None

    def handle_tracepoint(self, tp):
        if self.__last_action is None:
            self.LOG.debug("[%s] Start at: %d. Next action in %d ticks", self.name, tp.time, self.__action_interval)
            self.__last_action = tp.time
            return

        elapsed = tp.time - self.__last_action
        if elapsed < self.__action_interval:
            return

        tail = elapsed % self.__action_interval

        self.handle_action(int(elapsed / self.__action_interval))
        self.__last_action = tp.time - tail

    def handle_action(self, elapsed): raise NotImplementedError()

class PEBSBase(ActionSimulation):

    def __init__(self, period, action_interval, name = 'PEBSBase'):
        super().__init__(action_interval, name)
        self.__period = period
        self.__op_counter = {}


    def handle_tracepoint(self, tp):
        ActionSimulation.handle_tracepoint(self, tp)
        if tp.op in self.__op_counter:
            self.__op_counter[tp.op] += 1
        else:
            self.__op_counter[tp.op]  = 1

        if self.__op_counter[tp.op] % self.__period != 0:
            return

        self.handle_pebs_tracepoint(tp)

    def handle_pebs_tracepoint(self, tp): raise NotImplementedError()


class SimplePEBS(PEBSBase):
    class PageStat:
        def __init__(self):
            self.ops_counter = {}
            self.last_access = None

    def __init__(self, period, action_interval):
        super().__init__(period, action_interval)

        self.memory_map = {}

    def handle_pebs_tracepoint(self, tp):
        vpfn = pfn(tp.va)# + tp.op # encode operation in the offset bites
        if vpfn not in self.memory_map:
            self.memory_map[vpfn] = SimplePEBS.PageStat()

        if tp.op not in self.memory_map[vpfn].ops_counter:
            self.memory_map[vpfn].ops_counter[tp.op] = 0

        self.memory_map[vpfn].ops_counter[tp.op] += 1
        self.memory_map[vpfn].last_access = tp.time


    def handle_action(self, elapsed_intervals):
        self.LOG.debug("Action: elapsed %d period(s)", elapsed_intervals)
        for pfn in self.memory_map:
            page = self.memory_map[pfn]
            for op in page.ops_counter:
                count = page.ops_counter[op]
                page.ops_counter[op] >>= elapsed_intervals
                self.LOG.debug("{}({}):{}".format(pfn, op, count))

class HeMemPEBS(PEBSBase):

    PEBS_COOLING_THRESHOLD = 10
    HOT_READ_THRESHOLD  = 4
    HOT_WRITE_THRESHOLD = 4

    #SAMPLE_PERIOD = int(19997 / 30)

    HOT_RING_REQS_THRESHOLD = 1024*1024
    COLD_RING_REQS_THRESHOLD = 128

    INTERNAL_ACTION_PERIOD = 100

    PAGE_MASK = -1 << 21 # HUGE is used

    class PageStat:
        def __init__(self, va):
            self.accesses = { ord('r') : 0, ord('w') : 0 }
            self.va = va
            self.is_hot = False
            self.is_in_ring = False
            self.local_clock = None

        def below_threshold(self, sim,  op):
            if op in ['r', 'u']:
                return self.accesses[ord(op)] < sim.HOT_READ_THRESHOLD
            if op == 'w':
                return self.accesses[ord(op)] < sim.HOT_WRITE_THRESHOLD
            raise Exception("Unknown operation {}".format(op))

    def __init__(self, action_interval, period = 19997, name = None):
        if name is None:
            name = f'HeMem({period})'
        super().__init__(period, action_interval, name)

        self.pages = {}
        self.global_clock = 0
        self.make_hot_requests = []
        self.make_cold_requests = []

        self.cold_pages = []
        self.hot_pages = []

        self._accumulated_total = 0
        self.INTERNAL_ACTION_PERIOD = HeMemPEBS.INTERNAL_ACTION_PERIOD


    def handle_pebs_tracepoint(self, tp):
        #self.LOG.debug(tp)
        pfn = tp.va & self.PAGE_MASK
        if pfn not in self.pages:
            self.pages[pfn] = self.PageStat(pfn)
        page = self.pages[pfn]

        if tp.op not in page.accesses:
            page.accesses[tp.op] = 0

        page.accesses[tp.op] += 1
        if page.below_threshold(self, 'w') and page.below_threshold(self, 'r'):
            if page.is_hot and not page.is_in_ring:
                self.__make_cold_request(page)
        else:
            if not page.is_hot and not page.is_in_ring:
                self.__make_hot_request(page)

        if page.local_clock is not None:
            page.accesses[ord('r')] >>= (self.global_clock - page.local_clock)
            page.accesses[ord('w')] >>= (self.global_clock - page.local_clock)
        page.local_clock = self.global_clock

        if page.accesses[tp.op] > self.PEBS_COOLING_THRESHOLD:
            self.LOG.debug("Cooling threshold fired (%s == %d)", chr(tp.op), page.accesses[tp.op])
            self.global_clock += 1

    def handle_action(self, elapsed_intervals):
        self.LOG.debug("Action(elapsed %d time interval(s))", elapsed_intervals)

        total = 0
        num = 0
        for page in self.hot_pages:
            if page.is_in_ring: continue
            page.accesses[ord('w')] >>= (self.global_clock - page.local_clock)
            page.accesses[ord('r')] >>= (self.global_clock - page.local_clock)
            if page.below_threshold(self, 'r') and page.below_threshold(self, 'w'):
                self.__make_cold(page)
                num += 1
        if num > 0: self.LOG.debug("%d pages become cold", num)

        total += num
        num = 0
        while len(self.make_cold_requests) > 0 \
          and num < self.COLD_RING_REQS_THRESHOLD:
            page = self.make_cold_requests.pop()
            page.is_in_ring = False
            self.__make_cold(page)
            num += 1
        if num > 0: self.LOG.debug("%d cold pages requests processed", num)

        total += num
        num = 0
        while len(self.make_hot_requests) > 0 \
          and num < self.HOT_RING_REQS_THRESHOLD:
            page = self.make_hot_requests.pop()
            page.is_in_ring = False
            self.__make_hot(page)
            num += 1
        if num > 0: self.LOG.debug("%d hot page request(s) processed", num)

        self.LOG.debug("hot: %d; cold: %d; hot_rqs: %d; cold_rqs: %d",
                len(self.hot_pages),
                len(self.cold_pages),
                len(self.make_hot_requests),
                len(self.make_cold_requests))

        self._accumulated_total += total
        if self._accumulated_total > self.INTERNAL_ACTION_PERIOD:
            self.LOG.debug("accumulated total %d", self._accumulated_total)
            self.trigger_internal_action()
            self._accumulated_total = 0


    def __make_hot_request(self, page):
        page.is_in_ring = True
        self.make_hot_requests.append(page)
        self.LOG.debug("hot request")

    def __make_cold_request(self, page):
        page.is_in_ring = True
        self.make_cold_requests.append(page)
        self.LOG.debug("cold request")

    def __make_hot(self, page):
        page.is_hot = True
        if page in self.cold_pages:
            self.cold_pages.remove(page)
        if page not in self.hot_pages:
            self.hot_pages.append(page)

        #if self.migrate_func is not None and len(self.hot_pages) > self.MIGRATE_THRESHOLD:
        #    self.migrate_func(self)

    def __make_cold(self, page):
        page.is_hot = False
        if page in self.hot_pages:
            self.hot_pages.remove(page)
        if page not in self.cold_pages:
            self.cold_pages.append(page)


class Baseline(Simulation): # EventNumberDecay BaseLine
    #PAGE_MASK = -1 << 12
    DECAY_POWER = 1
    DECAY_THRESHOLD = 1000

    class PageStat:
        def __init__(self, va, time = 0):
            self.va = va
            self.local_clock = time
            self.ops = {}

        def sum_ops(self):
            return sum(self.ops.values())

        def __repr__(self):
            ops = ''
            for op,v in self.ops.items():
                ops += f'{op:c}-{v}'

            return f'PageStat[0x{self.va:x}]({self.local_clock}):{ops}'


    def __init__(self, name = 'Baseline'):
        super().__init__(name)

        self.heat_map = {}

        self._global_clock = 0

    def handle_tracepoint(self, tp):

        vpfn = tp.va & self.PAGE_MASK

        if vpfn not in self.heat_map:
            self.LOG.debug("New page {:x} with op '{:c}'".format(vpfn, tp.op))
            self.heat_map[vpfn] = self.PageStat(vpfn, self._global_clock)
            self.heat_map[vpfn].ops[tp.op] = 0

        page = self.heat_map[vpfn]
        if tp.op not in page.ops:
            self.LOG.debug("New operation {:c} at {:x}".format(tp.op, vpfn))
            page.ops[tp.op] = 0

        page.ops[tp.op] += 1

        if page.ops[tp.op] > self.DECAY_THRESHOLD:
            self.LOG.debug("Decay threshold fired")
            self._global_clock += 1

        self._adjust_page_temperature(page)

    def _adjust_page_temperature(self, page):
        decay_value = (self._global_clock - page.local_clock) * self.DECAY_POWER
        if decay_value == 0:
            return

        for op in page.ops:
            page.ops[op] >>= decay_value

        page.local_clock = self._global_clock

    def op_weight(self, op):
        return 1

    def page_temp(self, va, op = None):

        if isinstance(va, Baseline.PageStat):
            page = va
        else:
            vpfn = va & self.PAGE_MASK
            if vpfn not in self.heat_map:
                return 0

            page = self.heat_map[vpfn]

        self._adjust_page_temperature(page)

        if op is None:
            return sum([v * self.op_weight(op) \
                    for op,v in page.ops.items()])
        if op not in page.ops:
            return 0

        return page.ops[op] * self.get_wegith(op)

    #def active_pages(self):
    #    res = []
    #    for page in self.heat_map.values():
    #        #self._adjust_page_temperature(page)
    #        for op, count in page.ops.items():
    #            if count > 0:
    #                res.append(page)
    #                break
    #    return res

    def flush_temperature(self):
        for p in self.heat_map.values():
            self._adjust_page_temperature(p)

    def cold_page_number(self):
        return sum(1 for v in self.heat_map.values() if v.sum_ops() == 0)

    def pages_by_temp(self, op = None, reverse = False):
        return sorted(self.heat_map.values(), reverse = not reverse,
                key = lambda p: self.page_temp(p, op))


class TimeDecayBaseline(Simulation):
    class PageStat:
        def __init__(self, time = 0):
            self.last_decay = time
            self.count = 0

    def __init__(self, decay_window, decay_factor = 2):
        super().__init__('TimeDecayBaseLine')
        self.decay_factor = decay_factor
        self.decay_window = decay_window
        self.last_trace_time = None
        self.heat_map = {}

        self.__known_ops = set()

    def handle_tracepoint(self, tp):
        self.__known_ops.add(tp.op)

        vpfn = pfn(tp.va) + tp.op

        if vpfn not in self.heat_map:
            self.heat_map[vpfn] = self.PageStat(tp.time)
            self.LOG.debug("New page {:x}".format(vpfn))
        else:
            self.LOG.debug("Access page {:x} ({}[{}])".format(vpfn, tp.time, tp.time - self.heat_map[vpfn].last_decay))

        self.heat_map[vpfn].count += 1

        self.last_trace_time = tp.time
        self.adjust_page_temperature(vpfn)

    def adjust_page_temperature(self, vpfn):
        now = self.last_trace_time
        page_stat = self.heat_map[vpfn]
        interval = now - page_stat.last_decay
        if interval >= self.decay_window:
            self.LOG.debug("Adjusting heat {:x}".format(vpfn))
            page_stat.last_decay = now - (interval % self.decay_window)

            decay = self.decay_factor * int(interval / self.decay_window)
            page_stat.count = int(page_stat.count / decay)
            #if page_stat.count < decay:
            #    page_stat.count = 0
            #else:
            #    page_stat.count -= decay
        #else:
        #    self.LOG.debug("Interval {} < window ({})".format(interval, self.decay_window))

    def page_temp(self, va, op = None):
        if op is None:
            l = len(self.__known_ops)
            if   l == 0: op = 0
            elif l == 1: op = next(iter(self.__known_ops))
            else:        raise Exception("Can't guess the operation, should be specified explicitly")

        vpfn = pfn(va) + op
        if vpfn not in self.heat_map:
            return 0
        p = self.heat_map[vpfn]
        self.adjust_page_temperature(vpfn)
        return p.count

    def active_pages(self):
        res = []
        for page in self.heat_map.values():
            if page.count > 0:
                res.append(page)
        return res


class PageTableScan(ActionSimulation):

    class PageInfo:
        def __init__(self, va):
            self.va = va
            self.dirty = False
            self.access = False

    class SampleTimer(ActionSimulation):
        def __init__(self, ptscan, interval):
            super().__init__(interval, 'SampleTimer')
            self._ptscan = ptscan
            self.is_running = False

        def run_timer(self):
            self.reset()
            self.is_running = True

        def handle_action(self, elapsed):
            self.is_running = False
            self._ptscan.handle_sampling_finished(elapsed)

    def __init__(self,
            sample_period = 10**9, # 1s
            sample_duration = 100 * 10**3, # 100μs
            name = None):
        if name is None:
            name = f'PTS({sample_duration/1000}μs)'
        super().__init__(sample_period, name)

        self.PAGE_MASK = PageTableScan.PAGE_MASK
        self.sample_timer = self.SampleTimer(self, sample_duration)

        self.mmap = {}

    def handle_tracepoint(self, tp): # Simulation
        #self.LOG.debug("%s", tp)
        pfn = tp.va & self.PAGE_MASK
        pinfo = self.mmap.setdefault(pfn, self.PageInfo(pfn))
        chop = chr(tp.op)
        if chop in ['w']:
            pinfo.dirty = True
        elif chop in ['r', 'u']:
            pinfo.access = True
        else:
            raise Exception(f'Unknown operation \'{chop}\'')

        ActionSimulation.handle_tracepoint(self, tp)

        if self.sample_timer.is_running:
            self.sample_timer.handle_tracepoint(tp)

    def handle_action(self, elapsed): # ActionSimulation
        self.sample_timer.run_timer()
        self.handle_sampling_started()

    # API:
    def handle_sampling_started(self):
        self.LOG.debug("Sampling is started")
        self.mmap = {}

    def handle_sampling_finished(self, elapsed):
        self.LOG.debug("Sampling is finished")
        self.hot_pages = self.mmap.values()
        self.trigger_internal_action()


class LRU(Simulation):
    class PageInfo:
        def __init__(self, va):
            self.va = va
            self.dirty = False
            self.access = False
            self.naccesses = 0

        def clear_bits(self):
            self.dirty = self.access = False

    class ScanTimer(ActionSimulation):
        def __init__(self, lru, period):
            super().__init__(period, name = 'LRU_ScanTimer')
            self._lru = lru

        def handle_action(self, elapsed):
            self._lru._handle_scan_timer(elapsed)

    class CleanAccessBitTimer(ActionSimulation):
        def __init__(self, lru, clock_period = 20 * 10**6): #ms
            super().__init__(clock_period, name = 'LRU_CleanAccessBitTimer')
            self._lru = lru

        def handle_action(self, elapsed):
            self._lru._handle_clean_access_bit_timer(elapsed)

    class SwapTimer(ActionSimulation):
        def __init__(self, lru, period):
            super().__init__(period, name = 'LRU_SwapTimer')
            self._lru = lru

        def handle_action(self, elapsed):
            self._lru._handle_swap_timer(elapsed)

    def __init__(self,
            scan_period = 50 * 10**6, # 50ms
            swap_period =  1 * 10**9, # 1s
            name = 'LRU'):
        super().__init__(name)
        self._scan_timer = LRU.ScanTimer(self, scan_period)
        self._swap_timer = LRU.SwapTimer(self, swap_period)
        self._clean_timer = LRU.CleanAccessBitTimer(self)
        self._active_list = []
        self._inactive_list = []
        self._written_list = []
        self.mmap = {}

    @property
    def hot_pages(self):
        self.LOG.info("w: %d; a: %d: i: %d", len(self._written_list), len(self._active_list), len(self._inactive_list))
        return self._written_list + self._active_list

    def handle_tracepoint(self, tp): # Simulation
        self._handle_page_access(tp)
        self._scan_timer.handle_tracepoint(tp)
        self._swap_timer.handle_tracepoint(tp)
        self._clean_timer.handle_tracepoint(tp)

    def _handle_page_access(self, tp):
        pfn = tp.va & self.PAGE_MASK
        pinfo = self.mmap.setdefault(pfn, self.PageInfo(None))

        chop = chr(tp.op)
        if chop in ['w']:
            pinfo.dirty = True
        elif chop in ['r', 'u']:
            pass
        else:
            raise Exception(f'Unknown operation \'{chop}\'')

        pinfo.access = True

        if pinfo.va is None:
            pinfo.va = pfn
            self._active_list.append(pinfo)

    def _handle_clean_access_bit_timer(self, elapsed):
        for p in self.mmap.values():
            p.access = False

    #def _handle_scan_timer(self, elapsed):
    #    for p in  self._active_list + self._inactive_list:
    #        p.clear_bits()


    def _handle_scan_timer(self, elapsed):
        # check writes 
        l = len(self._written_list)
        while l > 0:
            p = self._written_list.pop(0)
            if p.access:
                if p.dirty:
                    self._written_list.append(p)
                    p.clear_bits()
                else:
                    self._active_list.append(p)
            else:
                self._inactive_list.append(p)
            l -= 1

        # shrink caches
        l = len(self._active_list)
        while l > 0:
            p = self._active_list.pop(0)
            if p.access:
                if p.dirty:
                    # page was written, so put it in the highest priority
                    # written list for memory type;
                    self._written_list.append(p)
                else:
                    # page was not written but was already in active list,
                    # keep it in active list since it was accessed
                    self._active_list.append(p)
                p.clear_bits()
            else:
                # found a cold page, put it to the inactive list
                p.naccesses = 0
                self._inactive_list.append(p)
            l -= 1

        # expand caches
        l = len(self._inactive_list)
        while l > 0:
            p = self._inactive_list.pop(0)
            if p.access:
                if p.dirty:
                    self._written_list.append(p)
                else:
                    # page was not written and used to be inactive
                    # non write-intensive pages must be accessed
                    # multiple time in a row to be marked active
                    if p.naccesses > 3:
                        p.naccesses = 0
                        self._active_list.append(p)
                    else:
                        p.clear_bits()
                        p.naccesses += 1
                        self._inactive_list.append(p)
            else:
                p.naccesses = 0
                self._inactive_list.append(p)
            l -= 1

    def _handle_swap_timer(self, elapsed):
        self.trigger_internal_action()

##### UNIT TESTS ##############################################################
if __name__ == '__main__':
    import unittest
    class Test_TimeDecayBaseline(unittest.TestCase):
        def test_smoke(self):
            #logging.basicConfig(level = logging.DEBUG)
            sim = TimeDecayBaseline(decay_factor = 2, decay_window = 3)

            sim.handle_tracepoint(Trace(0, 0x0500))
            sim.handle_tracepoint(Trace(0, 0x0600))
            sim.handle_tracepoint(Trace(0, 0x0700))
            self.assertEqual(sim.page_temp(0x0500), 3)
            self.assertEqual(sim.page_temp(0x1500), 0)

            sim.handle_tracepoint(Trace(1, 0x1200))
            self.assertEqual(sim.page_temp(0x0500), 3)
            self.assertEqual(sim.page_temp(0x1500), 1)

            sim.handle_tracepoint(Trace(2, 0x0300))
            self.assertEqual(sim.page_temp(0x0500), 4)
            self.assertEqual(sim.page_temp(0x1500), 1)
            self.assertEqual(sim.page_temp(0x2000), 0)

            sim.handle_tracepoint(Trace(3, 0x0123)) # Decay for 0
            self.assertEqual(sim.page_temp(0x0000), 2)
            self.assertEqual(sim.page_temp(0x1500), 1)

            sim.handle_tracepoint(Trace(4, 0x0456)) # Decay for 1
            self.assertEqual(sim.page_temp(0x0000), 3)
            self.assertEqual(sim.page_temp(0x1000), 0)

            sim.handle_tracepoint(Trace(5, 0x3300))
            self.assertEqual(sim.page_temp(0x0000), 3)
            self.assertEqual(sim.page_temp(0x1000), 0)
            self.assertEqual(sim.page_temp(0x2000), 0)
            self.assertEqual(sim.page_temp(0x3000), 1)

            sim.handle_tracepoint(Trace(6, 0x3300)) # Decay for 0
            self.assertEqual(sim.page_temp(0x0000), 1)
            self.assertEqual(sim.page_temp(0x1000), 0)
            self.assertEqual(sim.page_temp(0x2000), 0)
            self.assertEqual(sim.page_temp(0x3000), 2)

            sim.handle_tracepoint(Trace(7, 0x3300))
            self.assertEqual(sim.page_temp(0x0000), 1)
            self.assertEqual(sim.page_temp(0x1000), 0)
            self.assertEqual(sim.page_temp(0x2000), 0)
            self.assertEqual(sim.page_temp(0x3000), 3)

            sim.handle_tracepoint(Trace(8, 0x3333)) # Decay for 3
            self.assertEqual(sim.page_temp(0x0000), 1)
            self.assertEqual(sim.page_temp(0x1000), 0)
            self.assertEqual(sim.page_temp(0x2000), 0)
            self.assertEqual(sim.page_temp(0x3000), 2)

    class Test_PEBS(unittest.TestCase):
        def test_SimplePEBS(self):
            #logging.basicConfig(level = logging.DEBUG)
            sim = SimplePEBS(period = 2, action_interval = 2)
            # # - pebs captured
            # * - action triggered
            sim.handle_tracepoint(Trace( 0, 0x0500))
            sim.handle_tracepoint(Trace( 0, 0x0500)) #
            sim.handle_tracepoint(Trace( 0, 0x0501))
            sim.handle_tracepoint(Trace( 0, 0x1502)) #
            sim.handle_tracepoint(Trace( 2, 0x0500))
            sim.handle_tracepoint(Trace( 2, 0x0500)) # *
            sim.handle_tracepoint(Trace( 3, 0x0500))
            sim.handle_tracepoint(Trace( 4, 0x2500)) # *
            sim.handle_tracepoint(Trace( 5, 0x0500))
            sim.handle_tracepoint(Trace(15, 0x0500)) # *

        def test_HeMemPEBS(self):
            sim = HeMemPEBS(period = 1, action_interval = 3)
            #sim.LOG.setLevel(DEBUG)
            sim.PEBS_COOLING_THRESHOLD = 5
            sim.PAGE_MASK = -1
            sim.HOT_READ_THRESHOLD = 3
            sim.HOT_WRITE_THRESHOLD = 3

            sim.handle_tracepoint(Trace(0, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(1, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(2, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(2, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(3, 0x2, op = ord('r'))) # Action (before trace)
            self.assertEqual(len(sim.hot_pages), 1)
            self.assertEqual(len(sim.cold_pages), 0)

            sim.handle_tracepoint(Trace(4, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(5, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(6, 0x2, op = ord('r'))) # Action
            self.assertEqual(len(sim.hot_pages), 2)
            self.assertEqual(len(sim.cold_pages), 0)

            sim.handle_tracepoint(Trace(7, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(8, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(9, 0x1, op = ord('r'))) # Action
            self.assertEqual(len(sim.hot_pages), 1)
            self.assertEqual(len(sim.cold_pages), 1)

            sim.handle_tracepoint(Trace(10, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(10, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(11, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(11, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(12, 0x2, op = ord('r'))) # Action
            self.assertEqual(len(sim.hot_pages), 2)
            self.assertEqual(len(sim.cold_pages), 0)

            sim.handle_tracepoint(Trace(13, 0x2, op = ord('r')))
            sim.handle_tracepoint(Trace(14, 0x1, op = ord('r')))
            sim.handle_tracepoint(Trace(15, 0x3, op = ord('r'))) # Action
            self.assertEqual(len(sim.hot_pages), 1)
            self.assertEqual(len(sim.cold_pages), 1)

            sim.handle_tracepoint(Trace(16, 0x3, op = ord('r')))
            sim.handle_tracepoint(Trace(16, 0x3, op = ord('r')))
            sim.handle_tracepoint(Trace(17, 0x3, op = ord('r')))
            sim.handle_tracepoint(Trace(17, 0x3, op = ord('r')))
            sim.handle_tracepoint(Trace(17, 0x3, op = ord('r')))
            sim.handle_tracepoint(Trace(18, 0x3, op = ord('r'))) # Action
            self.assertEqual(len(sim.hot_pages), 1)
            self.assertEqual(len(sim.cold_pages), 2)

    class Test_Baseline(unittest.TestCase):
        def test_smoke(self):
            sim = Baseline()
            #sim.LOG.setLevel(DEBUG)
            sim.DECAY_THRESHOLD = 3
            sim.PAGE_MASK = -1 << 4

            sim.handle_tracepoint(Trace(0, 0x01))
            sim.handle_tracepoint(Trace(0, 0x02))
            sim.handle_tracepoint(Trace(0, 0x03))
            self.assertEqual(3, sim.page_temp(0x00))

            sim.handle_tracepoint(Trace(0, 0x04)) # decay
            self.assertEqual(2, sim.page_temp(0x01))

            sim.handle_tracepoint(Trace(0, 0x10))
            sim.handle_tracepoint(Trace(0, 0x11))
            sim.handle_tracepoint(Trace(0, 0x12))
            sim.handle_tracepoint(Trace(0, 0x13)) #decay
            self.assertEqual(1, sim.page_temp(0x00))
            self.assertEqual(2, sim.page_temp(0x10))

        def test_cold_page_count(self):
            sim = Baseline()
            #sim.LOG.setLevel(DEBUG)
            sim.DECAY_THRESHOLD = 2
            sim.PAGE_MASK = -1

            sim.handle_tracepoint(Trace(0, 0x1))
            sim.handle_tracepoint(Trace(1, 0x2))
            sim.handle_tracepoint(Trace(2, 0x3))
            sim.handle_tracepoint(Trace(3, 0x3))
            sim.handle_tracepoint(Trace(4, 0x3))
            sim.flush_temperature()
            self.assertEqual(2, sim.cold_page_number())
            #self.assertEqual(1, len(sim.active_pages()))

        def test_pages_by_temp(self):
            sim = Baseline()
            #sim.LOG.setLevel(DEBUG)
            sim.PAGE_MASK = -1
            sim.handle_tracepoint(Trace(0, 0x4))
            sim.handle_tracepoint(Trace(1, 0x3))
            sim.handle_tracepoint(Trace(2, 0x4))
            sim.handle_tracepoint(Trace(3, 0x2))
            sim.handle_tracepoint(Trace(4, 0x4))
            sim.handle_tracepoint(Trace(5, 0x3))
            sim.handle_tracepoint(Trace(6, 0x2))
            sim.handle_tracepoint(Trace(7, 0x1))
            sim.handle_tracepoint(Trace(8, 0x3))
            sim.handle_tracepoint(Trace(9, 0x4))

            idx = 5
            for page in sim.pages_by_temp():
                idx -= 1
                self.assertTrue(idx == page.va == sim.page_temp(page.va))
            self.assertEqual(1, idx)

            idx = 0
            for page in sim.pages_by_temp(reverse = True):
                idx += 1
                self.assertTrue(idx == page.va == sim.page_temp(page.va))
            self.assertEqual(4, idx)

    class Test_PTScan(unittest.TestCase):
        def test_smoke(self):
            sim = PageTableScan(1000, 10)
            sim.PAGE_MASK = -1
            #sim.LOG.setLevel(DEBUG)
            #sim.sample_timer.LOG.setLevel(DEBUG)

            sim.handle_tracepoint(Trace(   0, 0x1))
            sim.handle_tracepoint(Trace(  10, 0x2))
            sim.handle_tracepoint(Trace( 100, 0x3))
            self.assertEqual(3, len(sim.mmap))

            sim.handle_tracepoint(Trace(1000, 0x4))
            self.assertTrue(sim.sample_timer.is_running)
            self.assertEqual(0, len(sim.mmap))

            sim.handle_tracepoint(Trace(1001, 0x5))
            sim.handle_tracepoint(Trace(1005, 0x6))
            self.assertTrue(sim.sample_timer.is_running)
            self.assertEqual(2, len(sim.mmap))

            sim.handle_tracepoint(Trace(1010, 0x7))
            self.assertFalse(sim.sample_timer.is_running)
            sim.handle_tracepoint(Trace(1100, 0x8))
            self.assertFalse(sim.sample_timer.is_running)

            sim.handle_tracepoint(Trace(2001, 0x1))
            self.assertTrue(sim.sample_timer.is_running)
            self.assertEqual(0, len(sim.mmap))
            sim.handle_tracepoint(Trace(2009, 0x2))
            sim.handle_tracepoint(Trace(2009, 0x3))
            sim.handle_tracepoint(Trace(2009, 0x4))
            self.assertTrue(sim.sample_timer.is_running)
            self.assertEqual(3, len(sim.mmap))
            sim.handle_tracepoint(Trace(2210, 0x5))
            self.assertFalse(sim.sample_timer.is_running)
            self.assertEqual(4, len(sim.mmap))
            sim.handle_tracepoint(Trace(2211, 0x6))
            self.assertFalse(sim.sample_timer.is_running)

            sim.handle_tracepoint(Trace(3000, 0x3))

    class Test_LRU(unittest.TestCase):
        def test_smoke(self):
            sim = LRU(10, 100)
            sim.PAGE_MASK = -1

            class ChkAction:
                def __init__(self):
                    self.fired = False
                def __call__(self, sim):
                    self.fired = True
            action = ChkAction()

            sim.LOG.setLevel(DEBUG)
            sim.set_internal_action_handler(action)

            sim.handle_tracepoint(Trace(0, 0x1))
            self.assertEqual(1, len(sim.hot_pages))
            self.assertFalse(action.fired)

            sim.handle_tracepoint(Trace(2, 0x2))
            self.assertEqual(2, len(sim.hot_pages))
            self.assertFalse(action.fired)

            sim.handle_tracepoint(Trace(10, 0x2))
            sim.handle_tracepoint(Trace(11, 0x2))
            sim.handle_tracepoint(Trace(13, 0x2))
            sim.handle_tracepoint(Trace(20, 0x2))
            self.assertEqual(1, len(sim.hot_pages))

            sim.handle_tracepoint(Trace(100, 0x2))
            self.assertTrue(action.fired)

    logging.basicConfig(level = INFO, format='%(levelname)s:[%(filename)s:%(lineno)d] %(message)s')
    unittest.main()
################################################################################
