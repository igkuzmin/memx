import re
import bisect

def page(add):
    if add is None: return None
    return add >> 12

class Range:
    def __init__(self, l = None, r = None, v = None):
        self.left  = l
        self.right = r
        self.value = v

    def __repr__(self):
        return "[{:x},{:x}]({})".format(self.left, self.right, self.value)

class Iomem:
    def __init__(self, path = None, data = None):
        if (path is None and data is None) or \
           (path is not None and data is not None):
            raise Exception("Either path or data should be defined")

        self.__map = []

        if data is None:
            with open(path, 'r') as f:
                self.__parse(f.readlines())
        else:
            self.__parse(data.splitlines())


    def __parse(self, lines):

        for l in lines:
            m = re.split('-|:', l.strip(), 2)
            if len(m) != 3: next
            r = Range(int(m[0].strip(), 16), int(m[1].strip(), 16), m[2].strip())
            i = bisect.bisect_left(self.__map, r.left, key = lambda r: r.left)
            #if i != len(self.__map):
            #    old = self.__map[i]
            #    if old.left == r.left or old.right < r.left:
            #        raise Exception("Ranges are overlapped {} {}".format(old, r))
            self.__map.insert(i, r)
            #print("+ {} {}".format(i, r))

    def __getitem__(self, addr):

        #for i in self.__map:
        #    print(i)

        l = bisect.bisect_left(self.__map, addr, key = lambda r: r.left)
        if l == len(self.__map): return None

        #print ("{:x} - {}".format(addr, l))

        if addr == self.__map[l].left:
            r = self.__map[l]
        else:
            r = self.__map[l - 1]

        if r.left > addr or r.right < addr:
            return None

        return r.value

class PMaps:
    class Mapping:
        def __init__(self, left = None, right = None, pathname = None, perms = None, offset = None, dev = None, inode = None):
            self.left = left
            self.right = right
            self.pathname = pathname
            self.perms = perms
            self.offset = offset
            self.dev = dev
            self.inode = inode

        def is_empty(self):
            return self.left is None \
               and  self.right is None \
               and  self.pathname is None \
               and  self.perms is None \
               and  self.offset is None \
               and  self.dev is None \
               and  self.inode is None

    def __init__(self, path = None, data = None):
        if (path is None and data is None) or \
           (path is not None and data is not None):
            raise Exception("Either path or data should be defined")

        self.__map = []

        if data is None:
            with open(path, 'r') as f:
                self.__parse(f.readlines())
        else:
            self.__parse(data.splitlines())

    def __repr__(self):
        ret = "[{}]\n".format(len(self.__map))
        for m in self.__map:
            ret += "{:x}-{:x} {}\n".format(m.left, m.right, m.pathname)
        return ret

    def __parse(self, lines):
        prog = re.compile("\s*([0-9a-f]+)-([0-9a-f]+) ([rpxw-]{4}) ([0-9a-f]+) ([0-9a-f]+:[0-9a-f]+) (\d+)\s*(.*$)")
        for l in lines:
            m = prog.match(l)
            if not m: continue
            e = PMaps.Mapping(\
                left = int(m[1].strip(), 16),
                right = int(m[2].strip(), 16),
                perms = m[3].strip(),
                offset = int(m[4].strip(), 16),
                dev = m[5].strip(),
                inode = int(m[6].strip()),
                pathname = m[7].strip())

            i = bisect.bisect_left(self.__map, e.left, key = lambda k: k.left)
            self.__map.insert(i, e)

    def __getitem__(self, addr):
        l = bisect.bisect_left(self.__map, addr, key = lambda r: r.left)
        if l == len(self.__map): return PMaps.Mapping()
        if addr == self.__map[l].left:
            r = self.__map[l]
        else:
            r = self.__map[l - 1]

        if r.left > addr or r.right < addr:
            return PMaps.Mapping()

        return r


class Record:
    def __init__(self, va = None, pa = None, nu = 1):
        self.va = va
        self.pa = pa
        self.nu = nu

    def __repr__(self):
        if self.is_empty():
            return "{<empty>}"
        pa = ''
        va = ''
        if self.pa is not None: pa = '[{:x}]'.format(self.pa)
        if self.va is not None: va = '{:013x}'.format(self.va)
        return "{}{}x{}".format(va, pa, self.nu)

    def is_empty(self):
        return self.va is None and self.pa is None


class AddressSpace:
    def __init__(self, msk = 0x1000):
        self.__msk = msk

        self.__space = {}
        self.__sample_num = 0
        self.__va_low = None
        self.__va_high = None

    def __apply_mask(self, rec):
        msk = self.__msk
        va = None
        pa = None
        if rec.va is not None: va = int(rec.va / msk) * msk
        if rec.pa is not None: pa = int(rec.pa / msk) * msk
        new_rec = Record(va, pa, rec.nu)
        return new_rec

    def mask(self):
        return self.__msk

    def add(self, rec):
        r = self.__apply_mask(rec)
        if r.va in self.__space:
            p = self.__space[r.va]
            if p.pa in (None, 0):
                p.pa = r.pa
            elif r.pa not in (None, 0) and p.pa != r.pa:
                pass
                #raise Exception("Physical page mismatch for 0x{:x}: 0x{:x} != 0x{:x}".
                #        format(p.va, p.pa, r.pa))
            p.nu += r.nu

        else:
            self.__space[r.va] = r

        self.__sample_num += r.nu

        if self.__va_low is None or self.__va_low > r.va:
            self.__va_low = r.va

        if self.__va_high is None or self.__va_high < r.va:
            self.__va_high = r.va

    def values(self): return self.__space.values()
    def va_low(self): return self.__va_low
    def va_high(self): return self.__va_high

    def values_by_va(self):
        return sorted(self.values(), key = lambda rec: rec.va)
    def values_by_pa(self):
        return sorted(self.values(), key = lambda rec: rec.pa)
    def values_by_nu(self):
        return sorted(self.values(), key = lambda rec: rec.nu)

    def values_in_backets(self, n, b = None, e = None):
        if b is None: b = self.__va_low
        if e is None: e = self.__va_high

        bs = (e - b + 1) / n
        res = [0] * n
        for rec in self.__space.values():
            idx = int((rec.va - b) / bs)
            res[idx] += (rec.nu / self.__sample_num)
        return res

    def __getitem__(self, page):
        return self.__space[page]

    def len(self):
        return len(self.__space)

    def sample_num(self):
        return self.__sample_num

    def flush(self, width = 1000):
        n = self.sample_num()
        last = None
        dots = ''
        for v in self.values_by_va():
            if last is not None:
                d = int((v.va - last) / self.mask() - 1)
                if d == 1: dots += '.'
                elif d > 1: dots += '[{}]'.format(d)
            last = v.va
            if v.nu / n * width > 0.99:
                if len(dots) > 0:
                    print(dots)
                    dots = ''
                print("{:x}[{:x}]".format(v.va, v.pa), '@' * int(v.nu / n * width), v.nu)
            else:
                dots += '*'

##### UNIT TESTS ##############################################################
if __name__ == '__main__':
    import unittest
    class TestRecrod(unittest.TestCase):
        def test_constructor(self):
            r = Record(0x1)
            self.assertEqual(r.va, 0x1)
            self.assertEqual(r.pa, None)
            self.assertEqual(r.nu, 0x1)
            self.assertFalse(r.is_empty())

            r = Record(0x30, 0x7)
            self.assertEqual(r.va, 0x30)
            self.assertEqual(r.pa, 0x7)
            self.assertFalse(r.is_empty())

            r = Record()
            self.assertEqual(r.va, None)
            self.assertEqual(r.pa, None)
            self.assertTrue(r.is_empty())

        # -------------------------------------------------

    class TestAdressSpace(unittest.TestCase):
        def test_add(self):
            a = AddressSpace()
            a.add(Record(0x1))
            a.add(Record(0x2, 0x3000))
            a.add(Record(0x3))
            a.add(Record(0x3000))
            self.assertEqual(a.len(), 2)
            self.assertEqual(a[0].nu, 3)
            self.assertEqual(a[0].va, 0)
            self.assertEqual(a[0].pa, 0x3000)
            self.assertEqual(a.sample_num(), 4)

            l = a.values_by_va()
            self.assertEqual(len(l), 2)
            self.assertLess(l[0].va, l[1].va)
        # -------------------------------------------------

        def test_high_low(self):
            a = AddressSpace()
            a.add(Record(0x10000))
            a.add(Record(0x18000))
            a.add(Record(0x4000))
            a.add(Record(0x4000))
            self.assertEqual(a.va_high(), 0x18000)
            self.assertEqual(a.va_low(), 0x4000)
        # -------------------------------------------------

        def test_values_in_backets(self):
            a = AddressSpace()
            a.add(Record(0x1000))
            a.add(Record(0x2000))
            a.add(Record(0x4000))
            a.add(Record(0x5000))

            a.add(Record(0x3000))
            a.add(Record(0x3000))
            a.add(Record(0x3000))

            i = 0
            for v in a.values_in_backets(5):
                if i in [0, 1, 3, 4]: expected = 1/7
                elif i == 2: expected = 3/7
                self.assertEqual(v, expected)
                i += 1
        # -------------------------------------------------

        def test_values_by_mask(self):
            a = AddressSpace()
            a.add(Record(0x1000))
            a.add(Record(0x2000))
            a.add(Record(0x4000))
            a.add(Record(0x5000))

            a.add(Record(0x3000))
            a.add(Record(0x3000))
            a.add(Record(0x3000))

            i = 0
            for v in a.values_in_backets(5):
                if i in [0, 1, 3, 4]: expected = 1/7
                elif i == 2: expected = 3/7
                self.assertEqual(v, expected)
                i += 1
        # -------------------------------------------------

    class TestIomem(unittest.TestCase):
        data = \
            """ 00000000-00000fff : Reserved
                00001000-0005dff0 : System RAM
                0005e000-0005efff : Reserved 2
                0005f000-0009ffff : System RAM 2
                000a0000-000fffff : Reserved 3
                  000a0000-000bffff : PCI Bus 0000:00
                  000c0000-000cffff : Video ROM
                  000f0000-000fffff : System ROM
                00100000-5fcd4017 : System RAM
                5fcd4018-5fce4057 : System RAM
                5fce4058-5fce5017 : System RAM
                5fce5018-5fd0e657 : System RAM
                5fd0e658-622aafff : System RAM """

        def test_basic(self):
            iomem = Iomem(data = self.data)
            self.assertEqual(iomem[ 0x1055], 'System RAM')
            self.assertEqual(iomem[   0x55], 'Reserved')
            self.assertEqual(iomem[0x5f000], 'System RAM 2')
            self.assertEqual(iomem[0x9ffff], 'System RAM 2')
            self.assertEqual(iomem[0x5dff5], None)
            self.assertEqual(iomem[0xc0000], 'Video ROM')
            self.assertEqual(iomem[0xc0001], 'Video ROM')
            self.assertEqual(iomem[0xcffff], 'Video ROM')
            self.assertEqual(iomem[0xf0000], 'System ROM')
            self.assertEqual(iomem[0xf0005], 'System ROM')
            self.assertEqual(iomem[0xfffff], 'System ROM')

        # -------------------------------------------------

    class TestPMaps(unittest.TestCase):
        data = \
            """
            555555554000-555555559000 r--p 00000000 103:06 42083017                  /home/iku/src/ist/memx/memx
            555555559000-55555556d000 r-xp 00005000 103:06 42083017                  /home/iku/src/ist/memx/memx
            55555556d000-555555572000 r--p 00019000 103:06 42083017                  /home/iku/src/ist/memx/memx
            555555572000-555555573000 r--p 0001d000 103:06 42083017                  /home/iku/src/ist/memx/memx
            555555573000-555555574000 rw-p 0001e000 103:06 42083017                  /home/iku/src/ist/memx/memx
            555555574000-5555555c9000 rw-p 00000000 00:00 0                          [heap]
            7fff90000000-7fff90021000 rw-p 00000000 00:00 0 
            7ffff79a6000-7ffff79a8000 r--p 00000000 103:04 1458411                   /usr/lib/libdl-2.33.so
            7ffff79a8000-7ffff79aa000 r-xp 00002000 103:04 1458411                   /usr/lib/libdl-2.33.so
            """

        def test_basic(self):
            pmap = PMaps(data = self.data)
            self.assertEqual(pmap[0x555555554000].pathname, '/home/iku/src/ist/memx/memx')
            self.assertEqual(pmap[0x555555574000].pathname, '[heap]')
            self.assertEqual(pmap[0x7ffff79a6050].pathname, '/usr/lib/libdl-2.33.so')
            self.assertEqual(pmap[0x7ffff79aa001].is_empty(), True)
            self.assertEqual(pmap[0x7ffff79aa003].pathname, None)

        #def test_particular(self):
        #    pmap = PMaps("./memx.pmap")
        #    self.assertEqual(pmap[0x555555580000].pathname, '[heap]')

    unittest.main()
################################################################################
